module org.slovenlypolygon.musicbandui {
    requires javafx.media;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires com.jfoenix;
    requires se.michaelthelin.spotify;
    requires org.apache.httpcomponents.core5.httpcore5;
    requires java.net.http;
    requires nv.i18n;

    opens org.slovenlypolygon.musicBandApp.client.core to javafx.fxml;
    exports org.slovenlypolygon.musicBandApp.client.core;
    opens org.slovenlypolygon.musicBandApp.client.ui to javafx.fxml;
    opens org.slovenlypolygon.musicBandApp.client.ui.utils.filter;
    exports org.slovenlypolygon.musicBandApp.client.ui.utils.filter to javafx.fxml;
    exports org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene;
    opens org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene to javafx.fxml;
    exports org.slovenlypolygon.musicBandApp.client.ui.controllers.signScene;
    opens org.slovenlypolygon.musicBandApp.client.ui.controllers.signScene to javafx.fxml;
    exports org.slovenlypolygon.musicBandApp.client.core.helpers;
    opens org.slovenlypolygon.musicBandApp.client.core.helpers to javafx.fxml;
    exports org.slovenlypolygon.musicBandApp.client.ui.utils.models;
    opens org.slovenlypolygon.musicBandApp.client.ui.utils.models to javafx.fxml;
    exports org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.header;
    opens org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.header to javafx.fxml;
    exports org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window;
    opens org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window to javafx.fxml;
}