package org.slovenlypolygon.musicBandApp.client.ui.utils.filter;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public final class ExtendedPredicate<S> implements Predicate<S> {

	private Map<TableColumn<S, String>, Predicate<S>> tableColumn2Predicate = new HashMap<>();

	public ExtendedPredicate() {
	}

	public ExtendedPredicate(ExtendedPredicate<S> extendedPredicate) {
		this.tableColumn2Predicate = new HashMap<>(extendedPredicate.tableColumn2Predicate);
	}

	@Override
	public boolean test(S t) {
		for (Predicate<S> predicate : tableColumn2Predicate.values()) {
			if (predicate == null) {
				continue;
			}
			if (!predicate.test(t)) {
				return false;
			}
		}
		return true;
	}

	public void addPredicate(TableColumn<S, String> tableColumn, Predicate<S> predicate) {
		tableColumn2Predicate.put(tableColumn, predicate);
	}
}
