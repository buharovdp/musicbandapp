package org.slovenlypolygon.musicBandApp.client.ui.utils.models;

import com.jfoenix.controls.JFXButton;
import javafx.concurrent.Task;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import org.slovenlypolygon.musicBandApp.client.core.Client;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import se.michaelthelin.spotify.model_objects.specification.Track;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class TableModel {
    private String id;
    private String name;
    private String corX;
    private String corY;
    private String creationDate;
    private String numberOfParticipants;
    private String estabDate;
    private String genre;
    private String albumName;
    private String albumTracks;
    private String userName;

    private JFXButton button;
    private LocalDate trueEstabDate;
    private volatile boolean isPLaying = false;
    private MediaPlayer player;

    public TableModel(JFXButton btn, String id, String name, String corX, String corY, String creationDate, String numberOfParticipants, String estabDate, String genre, String albumName, String albumTracks, String userName) {
        try {
            this.button = btn;
            this.id = id;
            this.name = name;
            this.corX = corX;
            this.corY = corY;
            this.creationDate = creationDate;
            this.numberOfParticipants = numberOfParticipants;
            this.estabDate = estabDate;
            this.genre = genre;
            this.albumName = albumName;
            this.albumTracks = albumTracks;
            this.userName = userName;

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale());
            this.trueEstabDate = LocalDate.parse(estabDate, formatter);


            playButtonSetting(btn);
        } catch (NumberFormatException e) {
            throw new RuntimeException();
        }
    }

    private void playButtonSetting(JFXButton btn) {
        btn.setPrefWidth(24.0);
        btn.setPrefHeight(24.0);
        btn.setMaxHeight(24.0);
        btn.setMinHeight(24.0);
        btn.setMaxWidth(24.0);
        btn.setMinWidth(24.0);
        btn.setButtonType(JFXButton.ButtonType.RAISED);
        btn.setRipplerFill(Color.WHITE);
        Image playImg = new Image(Client.class.getResource("/icons/play.png").toExternalForm());
        ImageView imageView = new ImageView(playImg);
        imageView.setFitHeight(16.0);
        imageView.setFitWidth(16.0);
        btn.setStyle("-fx-alignment: CENTER; -fx-background-color: #EA3B6D; -fx-background-radius: 100");
        btn.setGraphic(imageView);

        btn.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            Image img = new Image(Client.class.getResource("/icons/stop.png").toExternalForm());

            if (isPLaying()) {
                img = new Image(Client.class.getResource("/icons/play.png").toExternalForm());
                SceneProcessor.getInstance().getTableViewController().stopAllMusic();
            } else {
                SceneProcessor.getInstance().getTableViewController().stopAllMusic();
                setPLaying(true, this.name, this.getAlbumName());
            }

            ImageView view = new ImageView(img);
            view.setFitHeight(16.0);
            view.setFitWidth(16.0);
            btn.setGraphic(view);
        });
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCorX() {
        return corX;
    }

    public String getCorY() {
        return corY;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getNumberOfParticipants() {
        return numberOfParticipants;
    }

    public String getEstabDate() {
        return estabDate;
    }

    public String getGenre() {
        return genre;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getAlbumTracks() {
        return albumTracks;
    }

    public String getUserName() {
        return userName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCorX(String corX) {
        this.corX = corX;
    }

    public void setCorY(String corY) {
        this.corY = corY;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public void setNumberOfParticipants(String numberOfParticipants) {
        this.numberOfParticipants = numberOfParticipants;
    }

    public void setEstabDate(String estabDate) {
        this.estabDate = estabDate;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public void setAlbumTracks(String albumTracks) {
        this.albumTracks = albumTracks;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDate getTrueEstabDate() {
        return trueEstabDate;
    }

    public void setPLaying(boolean PLaying, String bandName, String albumName) {
        isPLaying = PLaying;

        if (button != null) {
            Image img = new Image(Client.class.getResource("/icons/play.png").toExternalForm());
            ImageView view = new ImageView(img);
            view.setFitHeight(16.0);
            view.setFitWidth(16.0);
            button.setGraphic(view);
        }

        if (isPLaying) playMusic(bandName, albumName);
        else if (player != null) player.stop();
    }

    public boolean isPLaying() {
        return isPLaying;
    }

    public void playMusic(String bandName, String albumName) {
        new Thread(() -> {
            String previewTrackURL = SceneProcessor.getSpotifyAPI().findMusic(albumName, bandName);

            Media media = new Media(previewTrackURL);
            player = new MediaPlayer(media);
            player.play();
            player.setVolume(0.25);
            player.setOnEndOfMedia(() -> setPLaying(false, "", ""));
        }).start();
    }

    public void setButton(JFXButton button) {
        this.button = button;
    }

    public JFXButton getButton() {
        return button;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableModel model = (TableModel) o;
        return isPLaying == model.isPLaying && Objects.equals(id, model.id) && Objects.equals(name, model.name) && Objects.equals(corX, model.corX) && Objects.equals(corY, model.corY) && Objects.equals(creationDate, model.creationDate) && Objects.equals(numberOfParticipants, model.numberOfParticipants) && Objects.equals(estabDate, model.estabDate) && Objects.equals(genre, model.genre) && Objects.equals(albumName, model.albumName) && Objects.equals(albumTracks, model.albumTracks) && Objects.equals(userName, model.userName) && Objects.equals(button, model.button) && Objects.equals(trueEstabDate, model.trueEstabDate) && Objects.equals(player, model.player);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, corX, corY, creationDate, numberOfParticipants, estabDate, genre, albumName, albumTracks, userName, button, trueEstabDate, isPLaying, player);
    }
}
