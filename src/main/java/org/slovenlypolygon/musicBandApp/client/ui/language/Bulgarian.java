package org.slovenlypolygon.musicBandApp.client.ui.language;

import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Bulgarian extends Language {
    private final Map<String, String> words = new HashMap<>();
    private final Locale locale = new Locale("bg", "BG");
    private final String name = "Bulgarian";

    public Bulgarian() {
        // signIn screen
        words.put("sign-in", "Вход");
        words.put("login-form-in", "Потребителско име");
        words.put("password-form-in", "Парола");
        words.put("promote-text-in", "Нямате акаунт? ");
        words.put("sign-up-link", "Запиши Се");

        // signUp screen
        words.put("sign-up-title", "Създай акаунт");
        words.put("sign-up", "Запиши Се");
        words.put("login-form-up", "Потребителско име");
        words.put("password-form-up", "Парола");
        words.put("password-form-again-up", "Парола отново");
        words.put("promote-text-up", "Вече имате акаунт? ");
        words.put("sign-in-link", "Вход");

        // main screen
        // header
        words.put("sign-out", "Изход");
        words.put("language", "Език");
        words.put("commands", "Команди");
        words.put("execute-script", "Изпълнение на скрипт");

        // add screen
        words.put("add-title", "Добавяне на нов обект");
        words.put("name-form-add", "Име");
        words.put("x-form-add", "Х координата (номер)");
        words.put("y-form-add", "Y координата (номер)");
        words.put("number-of-participants-add", "Брой участници (брой)");
        words.put("creation-date-add", "Дата на установяване");
        words.put("genre-add", "Жанр");
        words.put("album-name-add", "Най-добро име на албум");
        words.put("album-tracks-add", "Песни в най-добър албум (брой)");
        words.put("create-element", "Създаване на елемент");

        // update screen
        words.put("update-title", "Редактиране на обект");
        words.put("name-form-update", "Име");
        words.put("x-form-update", "Х координата (номер)");
        words.put("y-form-update", "Y координата (номер)");
        words.put("number-of-participants-update", "Брой участници (брой)");
        words.put("creation-date-update", "Дата на установяване");
        words.put("genre-update", "Жанр");
        words.put("album-name-update", "Най-добро име на албум");
        words.put("album-tracks-update", "Песни в най-добър албум (брой)");
        words.put("update-element", "Прилагане на промените");
        words.put("remove-element", "Изтриване на елемент");

        // errors
        words.put("empty-login", "Вход не може да бъде празен");
        words.put("empty-password", "Паролата не може да е празна");
        words.put("password-not-match", "Паролите не съвпадат");
        words.put("invalid-login", "Невалидно потребителско име или парола");
        words.put("user-exists", "Такъв потребител вече съществува.");

        words.put("empty-band-name", "Името на групата не може да е празно");
        words.put("empty-x", "Координатата X трябва да е число.");
        words.put("empty-y", "Координатата Y трябва да е число.");
        words.put("empty-participants", "Броят на участниците трябва да бъде");
        words.put("empty-date", "Датата на учредяване трябва да бъде дата");
        words.put("empty-genre", "Изберете правилния жанр");
        words.put("empty-album", "Името на най-добрия албум не може да бъде празно");
        words.put("empty-tracks", "Броят на пътеките трябва да бъде");

        words.put("add-error", "Елемент не е добавен неизвестна грешка");
        words.put("owner-error", "Вие не сте собственик на този елемент.");
        words.put("not-changed", "Направете някои промени");

        // header windows
        words.put("apply", "Кандидатствай");
        words.put("path", "Път до файла");
        words.put("select", "Избор");
        words.put("execute", "Изпълнение");

        words.put("help", "Помощ");
        words.put("info", "Инфо");
        words.put("shuffle", "Разбъркване");
        words.put("reorder", "Пренареждане");

        // help window
        words.put("help-title", "Помощ");
        words.put("info-title", "Инфо");
        words.put("shuffle-title", "Разбъркване");
        words.put("reorder-title", "Пренареждане");

        words.put("help-info", "Показване на помощната информация");
        words.put("info-info", "Покажи информация за колекцията");
        words.put("shuffle-info", "Събиране на случайно разбъркване");
        words.put("reorder-info", "Пренареждане на колекция");
        words.put("clear-info", "Изчиства колекцията от елементи");
        words.put("add-info", "Добавяне на нов елемент към колекцията (за да изтриете или промените елемент, кликнете върху него в таблицата)");
        words.put("visual-info", "Отидете на визуален дисплей или се върнете на масата");

        // tableview columns
        words.put("filter", "Филтър");
        words.put("clear", "Изчистване");
        words.put("cancel", "Отказ");

        words.put("id", "Айди");
        words.put("name", "Име");
        words.put("creation", "Дата на създаване");
        words.put("participants", "Участници");
        words.put("estab-date", "Дата на установяване");
        words.put("genre", "Жанр");
        words.put("album", "Албум");
        words.put("tracks", "Тракове");
        words.put("user", "Потребител");

        // toasts
        words.put("invalid-command", "Невалидна команда в скрипта");
        words.put("script-executed", "Сценарият е изпълнен");
        words.put("file-error", "Файлът не е намерен или няма достъп до него");
        words.put("info-first", "Сега има ");
        words.put("info-second", " елементи в колекцията");

        // genres
        words.put("Genre", "Жанр");
        words.put("Hip hop", "Хип хоп");
        words.put("Rap", "Рэп");
        words.put("Post rock", "Пост рок");
        words.put("Punk rock", "Панк рок");
        words.put("Brit pop", "Брит поп");

        // genres to english
        words.put("Жанр-t", "Genre");
        words.put("Хип хоп-t", "Hip hop");
        words.put("Рэп-t", "Rap");
        words.put("Пост рок-t", "Post rock");
        words.put("Панк рок-t", "Punk rock");
        words.put("Брит поп-t", "Brit pop");

    }

    @Override
    public String get(String s) {
        return words.get(s);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public String getName() {
        return name;
    }
}
