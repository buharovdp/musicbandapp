package org.slovenlypolygon.musicBandApp.client.ui.language;

import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Russian extends Language {
    private final Map<String, String> words = new HashMap<>();
    private final Locale locale = new Locale("ru", "RU");
    private final String name = "Russian";

    public Russian() {
        // signIn screen
        words.put("sign-in", "Вход");
        words.put("login-form-in", "Логин");
        words.put("password-form-in", "Пароль");
        words.put("promote-text-in", "Нет аккаунта? ");
        words.put("sign-up-link", "Создать");

        // signUp screen
        words.put("sign-up-title", "Создание аккаунта");
        words.put("sign-up", "Регистрация");
        words.put("login-form-up", "Логин");
        words.put("password-form-up", "Пароль");
        words.put("password-form-again-up", "Повторите пароль");
        words.put("promote-text-up", "Уже есть аккаунт? ");
        words.put("sign-in-link", "Войти");

        // main screen
        // header
        words.put("sign-out", "Выход");
        words.put("language", "Язык");
        words.put("commands", "Команды");
        words.put("execute-script", "Скрипт файл");

        // add screen
        words.put("add-title", "Создание элемента");
        words.put("name-form-add", "Имя");
        words.put("x-form-add", "X координата (число)");
        words.put("y-form-add", "Y координата (число)");
        words.put("number-of-participants-add", "Количество участников (число)");
        words.put("creation-date-add", "Дата образования группы");
        words.put("genre-add", "Жанр");
        words.put("album-name-add", "Название лучшего альбома");
        words.put("album-tracks-add", "Количество треков в альбоме (число)");
        words.put("create-element", "Добавить элемент");

        // update screen
        words.put("update-title", "Редактирование элемента");
        words.put("name-form-update", "Имя");
        words.put("x-form-update", "X координата (число)");
        words.put("y-form-update", "Y координата (число)");
        words.put("number-of-participants-update", "Количество участников (число)");
        words.put("creation-date-update", "Дата образования группы");
        words.put("genre-update", "Жанр");
        words.put("album-name-update", "Название лучшего альбома");
        words.put("album-tracks-update", "Количество треков (число)");
        words.put("update-element", "Обновить");
        words.put("remove-element", "Удалить");

        // Errors
        words.put("empty-login", "Логин не может быть пустым");
        words.put("empty-password", "Пароль не может быть пустым");
        words.put("password-not-match", "Пароль не может быть пустым");
        words.put("invalid-login", "Неправильный логин или пароль");
        words.put("user-exists", "Такой пользователь уже существует");

        words.put("empty-band-name", "Название группы не может быть пустым");
        words.put("empty-x", "X координата должна быть числом");
        words.put("empty-y", "Y координата должна быть числом");
        words.put("empty-participants", "Количество участников должно быть числом");
        words.put("empty-date", "Дата создания группы должна быть датой");
        words.put("empty-genre", "Выберите жанр");
        words.put("empty-album", "Название лучшего альбома не может быть пустым");
        words.put("empty-tracks", "Количество треков в альбоме должно быть числом");

        words.put("add-error", "Элемент не был добавлен, неизвестная ошибка");
        words.put("owner-error", "Вы не являетесь владельцем этого элемента");
        words.put("not-changed", "Внесите какие-то изменения");

        // header windows
        words.put("apply", "Применить");
        words.put("path", "Путь до файла");
        words.put("select", "Выбрать");
        words.put("execute", "Выполнить");

        words.put("help", "Справка");
        words.put("info", "Инфо");
        words.put("shuffle", "Перемешать");
        words.put("reorder", "Перевернуть");

        // help window
        words.put("help-title", "Справка");
        words.put("info-title", "Инфо");
        words.put("shuffle-title", "Перемешать");
        words.put("reorder-title", "Перевернуть");

        words.put("help-info", "Показать справку");
        words.put("info-info", "Показать информацию о коллекции");
        words.put("shuffle-info", "Рандомно перемешать коллекцию");
        words.put("reorder-info", "Перевернуть коллекцию");
        words.put("clear-info", "Очистить коллекцию от ваших элементов");
        words.put("add-info", "Добавление нового элемента(для удаления или редактирования нажмите на элемент в таблице)");
        words.put("visual-info", "Перейти к визуальному отображению коллекции или вернуться назад к таблице");

        // tableview columns
        words.put("filter", "Фильтр");
        words.put("clear", "Сбросить");
        words.put("cancel", "Отмена");

        words.put("id", "Айди");
        words.put("name", "Имя");
        words.put("creation", "Дата добавления");
        words.put("participants", "Участники");
        words.put("estab-date", "Дата основания");
        words.put("genre", "Жанр");
        words.put("album", "Альбом");
        words.put("tracks", "Треки");
        words.put("user", "Юзер");

        // toasts
        words.put("invalid-command", "Неверная команда в скрипте");
        words.put("script-executed", "Скрипт был выполнен");
        words.put("file-error", "Файл не найден или к нему нет доступа");
        words.put("info-first", "Сейчас в коллекции ");
        words.put("info-second", " элементов");

        // genres
        words.put("Genre", "Жанр");
        words.put("Hip hop", "Хип хоп");
        words.put("Rap", "Рэп");
        words.put("Post rock", "Пост рок");
        words.put("Punk rock", "Панк рок");
        words.put("Brit pop", "Брит поп");

        // genres to english
        words.put("Жанр-t", "Genre");
        words.put("Хип хоп-t", "Hip hop");
        words.put("Рэп-t", "Rap");
        words.put("Пост рок-t", "Post rock");
        words.put("Панк рок-t", "Punk rock");
        words.put("Брит поп-t", "Brit pop");
    }

    @Override
    public String get(String s) {
        return words.get(s);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public String getName() {
        return name;
    }
}
