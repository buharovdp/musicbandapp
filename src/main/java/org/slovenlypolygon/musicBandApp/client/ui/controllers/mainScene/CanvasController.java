package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.common.models.MusicBand;

import java.net.URL;
import java.util.*;

public class CanvasController implements Initializable {
    @FXML
    private AnchorPane visualDisplay;

    private static final List<Color> trueColors = Arrays.asList(
            Color.rgb(234, 59, 109),
            Color.rgb(250, 160, 32),
            Color.rgb(138, 40, 250),
            Color.rgb(34, 177, 250),
            Color.rgb(175, 250, 0),
            Color.rgb(240, 59, 1),
            Color.rgb(105, 69, 250),
            Color.rgb(241, 106, 250),
            Color.rgb(250, 209, 5),
            Color.rgb(65, 255, 100),
            Color.rgb(40, 40, 40));

    private final Map<String, Color> colorsMap = new HashMap<>();
    private final Map<MusicBand, Canvas> visual = new HashMap<>();
    private final Set<Color> colorSet = new HashSet<>();
    private Stack<MusicBand> collection;
    private VisualController visualController = SceneProcessor.getInstance().getVisualController();

    public void start() {
        collection = SceneProcessor.getInstance().getCollection();
        fillColors();
        drawCollection();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setCanvasController(this);
    }

    private void fillColors() {
        Random rand = new Random();
        List<String> users = new ArrayList<>();

        for (MusicBand musicBand : collection) {
            if (!users.contains(musicBand.getUser())) users.add(musicBand.getUser());
        }

        for (int i = 0; i < users.size(); i++) {
            if (i < trueColors.size()) {
                colorSet.add(trueColors.get(i));
                colorsMap.put(users.get(i), trueColors.get(i));
            } else {
                while (true) {
                    int r = rand.nextInt(255);
                    int g = rand.nextInt(255);
                    int b = rand.nextInt(255);
                    Color color = Color.rgb(r, g, b);
                    if (!colorSet.contains(color)) {
                        colorSet.add(color);
                        colorsMap.put(users.get(i), color);
                        break;
                    }
                }
            }
        }
    }


    private synchronized void drawCollection() {
        int minCordX = getMinCordX();
        int minCordY = getMinCordY();
        for (MusicBand element : collection) {
            if (visualController == null) visualController = SceneProcessor.getInstance().getVisualController();
            Canvas canvas = visualController.generateCanvas(element, colorsMap.get(element.getUser()), minCordX, minCordY, visualDisplay);
            if (canvas != null) {
                visual.put(element, canvas);
                FadeTransition fadeTransition = new FadeTransition();
                fadeTransition.setDuration(Duration.millis(1000));
                fadeTransition.setFromValue(0);
                fadeTransition.setToValue(10);
                fadeTransition.setNode(canvas);
                fadeTransition.playFromStart();
                visualDisplay.getChildren().add(canvas);
            }
        }
    }

    private int getMinCordY() {
        int min = Integer.MAX_VALUE;
        for (MusicBand element : collection) {
            if (element.getCoordinates().getY() < min) min = element.getCoordinates().getY();
        }
        return min;
    }

    private int getMinCordX() {
        int min = Integer.MAX_VALUE;
        for (MusicBand element : collection) {
            if (element.getCoordinates().getX() < min) min = element.getCoordinates().getX();
        }
        return min;
    }

    private void redraw() {
        fillColors();
        drawCollection();
    }

    public void updateCanvas() {
        Stack<MusicBand> newCollection = SceneProcessor.getInstance().getCollection();
        visualDisplay.getChildren().clear();
        collection = newCollection;
        redraw();
    }

    public Map<MusicBand, Canvas> getVisual() {
        return visual;
    }

    public AnchorPane getVisualDisplay() {
        return visualDisplay;
    }
}
