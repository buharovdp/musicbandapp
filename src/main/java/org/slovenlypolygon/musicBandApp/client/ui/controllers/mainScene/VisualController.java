package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestCreator;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.HeaderWindowController;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window.AddController;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window.UpdateController;
import org.slovenlypolygon.musicBandApp.client.ui.utils.models.TableModel;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;
import org.slovenlypolygon.musicBandApp.common.models.MusicBand;
import org.slovenlypolygon.musicBandApp.common.utils.Request;

import java.net.URL;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class VisualController implements Initializable {
    @FXML
    private AnchorPane parentContainer;
    @FXML
    private AnchorPane container;

    @FXML
    private Button signOutButton;
    @FXML
    private Button languageButton;
    @FXML
    private Button commandsButton;
    @FXML
    private Button executeButton;

    @FXML
    private Label userName;
    @FXML
    private Button addButton;
    @FXML
    private Button clearButton;
    @FXML
    private Button tableButton;

    @FXML
    AnchorPane addWindowXML;
    @FXML
    AnchorPane updateWindowXML;
    @FXML
    BorderPane languageWindow;
    @FXML
    BorderPane commandsWindow;
    @FXML
    BorderPane executeWindow;

    @FXML
    private com.jfoenix.controls.JFXScrollPane scrollPane;

    private AddController addController;
    private UpdateController updateController;
    private HeaderWindowController languageController;
    private HeaderWindowController commandsController;
    private HeaderWindowController executeController;
    private HeaderWindowController helpController;

    private SceneProcessor sceneProcessor;
    private List<HeaderWindowController> headerControllerList = new ArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        sceneProcessor = SceneProcessor.getInstance();
        sceneProcessor.setVisualController(this);
        sceneProcessor.drawCanvas();
        sceneProcessor.getCanvasController().updateCanvas();
        redrawButtons();
        initializeControllers();

        handleEvents();
        setUsername();

        setEventHandler();
    }

    public void setEventHandler() {
        AnchorPane visualDisplay = sceneProcessor.getCanvasController().getVisualDisplay();
        for (Map.Entry<MusicBand, Canvas> pair : sceneProcessor.getCanvasController().getVisual().entrySet()) {
            Canvas canvas = pair.getValue();
            canvas.setOnMouseClicked(mouseEvent -> {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                        System.out.println("One clicked");
                        showInfo(pair.getKey());
                }
                if (mouseEvent.getButton().equals(MouseButton.SECONDARY)) {
                    System.out.println("Clicked");
                    visualDisplay.getChildren().remove(canvas);
                    visualDisplay.getChildren().add(0, canvas);
                }
            });
        }
    }

    private void initializeControllers() {
        addController = sceneProcessor.getAddController();
        updateController = sceneProcessor.getUpdateController();
        languageController = sceneProcessor.getLanguageController();
        commandsController = sceneProcessor.getCommandsController();
        executeController = sceneProcessor.getExecuteController();
        helpController = sceneProcessor.getHelpController();

        headerControllerList.add(languageController);
        headerControllerList.add(commandsController);
        headerControllerList.add(executeController);
        headerControllerList.add(helpController);
    }

    public void setUsername() {
        String login = sceneProcessor.getUser().getLogin();
        if (login == null) login = "not user";
        login = login.substring(0, 1).toUpperCase() + login.substring(1).toLowerCase();
        userName.setText(login);
    }

    private void handleEvents() {
        sceneProcessor.getStage().widthProperty().addListener((obs, oldVal, newVal) -> {
            parentContainer.setPrefWidth(newVal.doubleValue());
            executeWindow.setLayoutX(executeWindow.getLayoutX() + (newVal.doubleValue() - oldVal.doubleValue()));
            commandsWindow.setLayoutX(commandsWindow.getLayoutX() + (newVal.doubleValue() - oldVal.doubleValue()));
        });

        sceneProcessor.getStage().heightProperty().addListener((obs, oldVal, newVal) -> {
            parentContainer.setPrefHeight(newVal.doubleValue() - 32);
        });

        signOutButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeAnotherWindows(null);
            sceneProcessor.setCollection(null);
            sceneProcessor.openSignInScene(mouseEvent, parentContainer, container);
        });

        addButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeAnotherWindows(null);
            addController.openWindow();
        });

        clearButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            Request clearRequest = RequestCreator.getInstance().clearRequest(sceneProcessor.getUser());
            sceneProcessor.getSender().send(clearRequest);
            sceneProcessor.updateCollection();
        });

        tableButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeAnotherWindows(null);
            sceneProcessor.openMainScene(mouseEvent, parentContainer, container);
        });

        languageButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (languageWindow.isDisabled()) {
                closeAnotherWindows(languageController);
                languageController.openWindow();
            } else languageController.closeWindow();
        });

        commandsButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (commandsWindow.isDisabled()) {
                closeAnotherWindows(commandsController);
                commandsController.openWindow();
            } else commandsController.closeWindow();
        });

        executeButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (executeWindow.isDisabled()) {
                closeAnotherWindows(executeController);
                executeController.openWindow();
            } else executeController.closeWindow();
        });

        scrollPane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeAnotherWindows(null);
        });
    }

    public Canvas generateCanvas(MusicBand element, Color color, int minX, int minY, AnchorPane visualDisplay) {
        if (minX < 0) minX = -minX;
        if (minY < 0) minY = -minY;

        Canvas canvas = drawElement(color, element.getId());
        canvas.setLayoutX(minX + element.getCoordinates().getX());
        canvas.setLayoutY(minY + element.getCoordinates().getY());
//        canvas.setOnMouseClicked(mouseEvent -> showInfo(element));
        canvas.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                System.out.println("One clicked");
                showInfo(element);
            }
            if (mouseEvent.getButton().equals(MouseButton.SECONDARY)) {
                System.out.println("Clicked");
                visualDisplay.getChildren().remove(canvas);
                visualDisplay.getChildren().add(0, canvas);
            }
        });
        canvas.setOnMouseEntered(mouseEvent -> {
            canvas.setScaleX(1.05);
            canvas.setScaleY(1.05);
        });
        canvas.setOnMouseExited(mouseEvent -> {
            canvas.setScaleX(1);
            canvas.setScaleY(1);
        });
        return canvas;
    }


    private Canvas drawElement(Color color, int id) {
        Canvas can = new Canvas(50, 50);
        GraphicsContext gc = can.getGraphicsContext2D();
        gc.setFill(color);
        gc.setStroke(color);
        gc.fillRect(0, 0, 50, 50);
        gc.setFill(Color.WHITE);
        gc.fillText(String.valueOf(id),5, 16);
        can.setStyle("-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.3), 15, 0, 2, 4)");
        return can;
    }

    private void showInfo(MusicBand musicBand) {
        closeAnotherWindows(null);

        String id = String.valueOf(musicBand.getId());
        String name = musicBand.getName();
        String corX = String.valueOf(musicBand.getCoordinates().getX());
        String corY = String.valueOf(musicBand.getCoordinates().getY());
        String creationDate = musicBand.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));
        String numberOfParticipants = String.valueOf(musicBand.getNumberOfParticipants());
        String estabDate = musicBand.getEstablishmentDate().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));;
        String genre = musicBand.getGenre().toString();
        String albumName = musicBand.getBestAlbum().getName();
        String albumTracks = String.valueOf(musicBand.getBestAlbum().getTracks());
        String userName = musicBand.getUser();

        updateController.openWindow(new TableModel(new JFXButton(), id, name, corX, corY, creationDate, numberOfParticipants, estabDate, genre, albumName, albumTracks, userName));
    }

    private void closeAnotherWindows(HeaderWindowController controller) {
        for (HeaderWindowController c : headerControllerList) {
            if (controller != c) c.closeWindow();
        }
    }

    public void redrawButtons() {
        Language language = sceneProcessor.getLanguage();
        signOutButton.setText(language.get("sign-out"));
        languageButton.setText(language.get("language"));
        commandsButton.setText(language.get("commands"));
        executeButton.setText(language.get("execute-script"));

        if (addController != null) addController.redrawButtons();
        if (updateController != null) updateController.redrawButtons();

        for (HeaderWindowController controller : headerControllerList) {
            if (controller != null) controller.redrawButtons();
        }
    }
}

