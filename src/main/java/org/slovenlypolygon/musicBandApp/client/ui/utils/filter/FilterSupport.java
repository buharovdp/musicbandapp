package org.slovenlypolygon.musicBandApp.client.ui.utils.filter;

import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.TextAlignment;
import org.slovenlypolygon.musicBandApp.client.core.Client;

public final class FilterSupport {

    //	private static final String FILTER_BUTTON_CSS = FilterEventHandler.class.getResource("style.css").toString();
    private static final String FILTER_BUTTON_CSS = Client.class.getResource("/filter/style.css").toString();

    public static <S> void addFilter(TableColumn<S, String> column, boolean addWrapLabels) {
        if (column == null) {
            throw new IllegalArgumentException("TableColumn must not be null");
        }

        HBox hBox = new HBox();

        if (addWrapLabels) {
            Label label = new Label(column.getText());
            column.setText("");

            label.setStyle("-fx-padding: 8");
            label.setWrapText(true);
            label.setAlignment(Pos.CENTER);
            label.setTextAlignment(TextAlignment.CENTER);
            HBox.setHgrow(label, Priority.ALWAYS);
            hBox.setAlignment(Pos.CENTER);
            hBox.getChildren().add(label);
        }

        Button button = new Button();
        button.setFocusTraversable(false);
        button.getStylesheets().add(FILTER_BUTTON_CSS);
        button.setOnAction(new FilterEventHandler<S>(column, button));
//		column.setGraphic(button);

        hBox.getChildren().add(button);
        column.setGraphic(hBox);
    }

    public static <S> ObservableList<? extends S> getItems(TableView<S> tableView) {
        if (tableView == null) {
            throw new IllegalArgumentException("TableView must not be null");
        }

        ObservableList<S> items = tableView.getItems();
        return getUnwrappedList(items);
    }

    public static <S> ObservableList<? extends S> getUnwrappedList(ObservableList<S> wrappedList) {
        if (wrappedList instanceof SortedList) {
            if (((SortedList<S>) wrappedList).getSource() instanceof FilteredList) {
                FilteredList<? extends S> filteredList = (FilteredList<? extends S>) ((SortedList<? extends S>) wrappedList).getSource();
                ObservableList<? extends S> unwrappedList = filteredList.getSource();
                return unwrappedList;
            }
        }
        return wrappedList;
    }

}
