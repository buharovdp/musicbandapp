package org.slovenlypolygon.musicBandApp.client.ui.utils.filter;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.input.MouseButton;
import javafx.util.Callback;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.utils.filter.predicate.InStringExpressionPredicate;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;

public class FilterController<S> implements Initializable {

    @FXML
    private TextField filterTf;

    @FXML
    private ListView<String> filterLv;

    @FXML
    private Button filterButton;
    @FXML
    private Button clearButton;
    @FXML
    private Button cancelButton;

    private TableColumn<S, String> tableColumn;

    private FilterEventHandler<S> eventHandler;

    private ObservableList<S> items;
    private FilteredList<S> filteredList;
    private SortedList<S> sortedList;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setFilterController(this);
        filterLv.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2) {
                String selectedItem = filterLv.getSelectionModel().getSelectedItem();
                if (selectedItem != null) {
                    filterTf.setText(selectedItem);
                    filter();
                }
            }
        });

        redrawButtons();
    }

    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();

        filterButton.setText(language.get("filter"));
        clearButton.setText(language.get("clear"));
        cancelButton.setText(language.get("cancel"));
    }

    public void setColumn(TableColumn<S, String> tableColumn) {
        this.tableColumn = tableColumn;
        initializeLists();
    }

    private void initializeLists() {
        items = tableColumn.getTableView().getItems();
        wrapObservableList();

        if (!(items instanceof SortedList)) {
            throw new IllegalArgumentException("Something went wrong, unexpected List implementation in TableView");
        }

        sortedList = (SortedList<S>) items;
        filteredList = (FilteredList<S>) sortedList.getSource();
        items = (ObservableList<S>) filteredList.getSource();

        sortedList.comparatorProperty().bind(tableColumn.getTableView().comparatorProperty());
        tableColumn.getTableView().setItems(sortedList);
    }

    private void wrapObservableList() {
        items = tableColumn.getTableView().getItems();
        if (!(items instanceof SortedList)) {
            FilteredList<S> filteredListInt = items.filtered(null);
            SortedList<S> sortedInt = filteredListInt.sorted();
            tableColumn.getTableView().setItems(sortedInt);

            items = sortedInt;
        }
    }

    @FXML
    public void filter() {
        String filterText = filterTf.getText();
        Predicate<S> predicate;
        boolean filterActive;
        if (filterText.isEmpty()) {
            filterActive = false;
            predicate = null;
        } else {
            filterActive = true;
            predicate = new InStringExpressionPredicate.InStringTableColumnExpressionPredicate<>(tableColumn, filterText);
        }
        eventHandler.setFilterActive(filterActive);

        ExtendedPredicate<S> predicateExt = (ExtendedPredicate<S>) filteredList.getPredicate();
        if (predicateExt == null) {
            predicateExt = new ExtendedPredicate<>();
        }
        predicateExt.addPredicate(tableColumn, predicate);

        // Have to be copied for the framework to see a change
        predicateExt = new ExtendedPredicate<S>(predicateExt);
        filteredList.setPredicate(predicateExt);

        hide();
    }

    @FXML
    public void clear() {
        filterTf.clear();
        filter();
        eventHandler.setFilterActive(false);
    }

    public void show() {
        setFilteredItemsInFilterListView();
        filterTf.requestFocus();
        filterLv.getSelectionModel().clearSelection();
    }

    @FXML
    public void hide() {
        eventHandler.hidePopup();
    }

    private void setFilteredItemsInFilterListView() {
        // Initialize list view with unique values of this column
        Set<String> uniqueValues = getUniqueValuesFromColumn(filteredList);

        FilteredList<String> filteredUniqueValues = new FilteredList<>(FXCollections.observableArrayList(uniqueValues));
        SortedList<String> sortedUniqueValues = filteredUniqueValues.sorted(String.CASE_INSENSITIVE_ORDER);
        filterLv.setItems(sortedUniqueValues);

        // Filter list view if text is entered
        ChangeListener<String> listener = (ChangeListener<String>) (observable, oldValue, newValue) -> {
            filteredUniqueValues.setPredicate(new InStringExpressionPredicate(newValue));
        };
        filterTf.textProperty().addListener(listener);

        // fire event to filter the list
        listener.changed(filterTf.textProperty(), null, filterTf.getText());
    }

    private Set<String> getUniqueValuesFromColumn(FilteredList<S> filteredList) {
        ExtendedPredicate<S> predicate = (ExtendedPredicate<S>) filteredList.getPredicate();

        FilteredList<S> filteredListWithoutFilterOfThisColumn;
        if (predicate == null) {
            filteredListWithoutFilterOfThisColumn = filteredList;
        } else {
            // Remove Predicate of this column
            ExtendedPredicate<S> predicateCopy = new ExtendedPredicate<>(predicate);
            predicateCopy.addPredicate(tableColumn, null);
            filteredListWithoutFilterOfThisColumn = items.filtered(predicateCopy);
        }

        Set<String> uniqueItems = filteredListWithoutFilterOfThisColumn //
                .stream() //
                .map(p -> getStringValueOfColumn(p)) //
                .collect(Collectors.toSet());
        return uniqueItems;
    }

    private String getStringValueOfColumn(S item) {
        Callback<CellDataFeatures<S, String>, ObservableValue<String>> cellValueFactory = tableColumn.getCellValueFactory();
        CellDataFeatures<S, String> param = new CellDataFeatures<>(null, tableColumn, item);
        String value = cellValueFactory.call(param).getValue();

        return value;
    }

    public void tableDataChanged() {
        Predicate<? super S> predicate = filteredList.getPredicate();
        initializeLists();
        filteredList.setPredicate(predicate);
    }

    public void setHandler(FilterEventHandler<S> eventHandler) {
        this.eventHandler = eventHandler;
    }
}
