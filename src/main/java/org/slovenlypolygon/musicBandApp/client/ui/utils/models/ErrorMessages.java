package org.slovenlypolygon.musicBandApp.client.ui.utils.models;

import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;

public enum ErrorMessages {
    // success
    SUCCESS(""),
    // for sign in and sign up
    LOGIN("empty-login"),
    PASSWORD("empty-password"),
    REPEAT_PASSWORD("password-not-match"),
    NO_USER("invalid-login"),
    ALREADY_TAKEN("user-exists"),

    // for add and update
    BAND_NAME("empty-band-name"),
    COR_X("empty-x"),
    COR_Y("empty-y"),
    PARTICIPANTS("empty-participants"),
    ESTAB_DATE("empty-date"),
    GENRE("empty-genre"),
    BEST_ALBUM("empty-album"),
    TRACKS("empty-tracks"),


    ADD_ERROR("add-error"),
    UPDATE_ERROR("owner-error"),
    REMOVE_ERROR("owner-error"),
    NOT_CHANGED_ERROR("not-changed");

    private final String message;

    ErrorMessages(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return SceneProcessor.getInstance().getLanguage().get(message);
    }
}
