package org.slovenlypolygon.musicBandApp.client.ui.controllers;

import javafx.animation.FadeTransition;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.util.Duration;

public abstract class ValidatingController implements Initializable {
    protected final FadeTransition fadeInIncorrectPane = new FadeTransition(Duration.millis(200));
    protected final FadeTransition fadeOutIncorrectPane = new FadeTransition(Duration.millis(200));

    protected abstract void handleEvents();
    protected abstract void tryToExecute(Event event);
    protected abstract void animationInitializing();
    public void redrawButtons(){}
    
}
