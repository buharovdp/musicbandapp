package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.header;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.HeaderWindowController;
import org.slovenlypolygon.musicBandApp.client.ui.language.*;

import java.net.URL;
import java.util.ResourceBundle;

public class LanguageController extends HeaderWindowController {
    @FXML
    private BorderPane languageWindow;
    @FXML
    private ToggleGroup languageGroup;
    @FXML
    private Button applyLanguageButton;

    @FXML
    private RadioButton english;
    @FXML
    private RadioButton russian;
    @FXML
    private RadioButton portuguese;
    @FXML
    private RadioButton bulgarian;
    @FXML
    private RadioButton spanish;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setLanguageController(this);

        animationInitializing();
        handleEvents();
        redrawButtons();
    }

    protected void handleEvents() {
        applyLanguageButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            tryToExecute(mouseEvent);
        });
    }

    @Override
    protected void tryToExecute(Event event) {
        Toggle selectedToggle = languageGroup.getSelectedToggle();
        Language language;
        if (selectedToggle == english) {
            language = new English();
        } else if (selectedToggle == russian) {
            language = new Russian();
        } else if (selectedToggle == portuguese) {
            language = new Portuguese();
        } else if (selectedToggle == bulgarian) {
            language = new Bulgarian();
        } else if (selectedToggle == spanish) {
            language = new Spanish();
        } else {
            language = new English();
        }

        SceneProcessor.getInstance().getTableViewController().stopAllMusic();
        SceneProcessor.getInstance().setLanguage(language);
        SceneProcessor.getInstance().redrawScene();
        closeWindow();
    }


    public void closeWindow() {
        if (languageWindow.isDisabled()) return;
        fadeOutIncorrectPane.playFromStart();
        languageWindow.setDisable(true);
        languageWindow.setOpacity(0);
    }

    public void openWindow() {
        if (!languageWindow.isDisabled()) return;
        fadeInIncorrectPane.playFromStart();
        languageWindow.setDisable(false);
        languageWindow.setOpacity(1);

        languageGroup.selectToggle(getNowLanguage());
    }

    private RadioButton getNowLanguage() {
        switch (SceneProcessor.getInstance().getLanguage().getName()) {
            case "Russian":
                return russian;
            case "Portuguese":
                return portuguese;
            case "Bulgarian":
                return bulgarian;
            case "Spanish":
                return spanish;
            default:
                return english;
        }
    }

    @Override
    protected void animationInitializing() {
        fadeInIncorrectPane.setNode(languageWindow);
        fadeInIncorrectPane.setFromValue(0.0);
        fadeInIncorrectPane.setToValue(1.0);
        fadeInIncorrectPane.setCycleCount(1);
        fadeInIncorrectPane.setAutoReverse(false);

        fadeOutIncorrectPane.setNode(languageWindow);
        fadeOutIncorrectPane.setFromValue(1.0);
        fadeOutIncorrectPane.setToValue(0.0);
        fadeOutIncorrectPane.setCycleCount(1);
        fadeOutIncorrectPane.setAutoReverse(false);
    }

    @Override
    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();

        applyLanguageButton.setText(language.get("apply"));
    }
}
