package org.slovenlypolygon.musicBandApp.client.ui.language;

import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class English extends Language {
    private final Map<String, String> words = new HashMap<>();
    private final Locale locale = Locale.ENGLISH;
    private final String name = "English";

    public English() {
        // signIn screen
        words.put("sign-in", "Sign In");
        words.put("login-form-in", "Login");
        words.put("password-form-in", "Password");
        words.put("promote-text-in", "Don't have an account? ");
        words.put("sign-up-link", "Sign Up");

        // signUp screen
        words.put("sign-up-title", "Create an account");
        words.put("sign-up", "Sign Up");
        words.put("login-form-up", "Login");
        words.put("password-form-up", "Password");
        words.put("password-form-again-up", "Password again");
        words.put("promote-text-up", "Already have an account? ");
        words.put("sign-in-link", "Sign In");

        // main screen
        // header
        words.put("sign-out", "Sign Out");
        words.put("language", "Language");
        words.put("commands", "Commands");
        words.put("execute-script", "Execute script");

        // add screen
        words.put("add-title", "Adding a new object");
        words.put("name-form-add", "Name");
        words.put("x-form-add", "X coordinate (number)");
        words.put("y-form-add", "Y coordinate (number)");
        words.put("number-of-participants-add", "Number of participants (number)");
        words.put("creation-date-add", "Establishment date");
        words.put("genre-add", "Genre");
        words.put("album-name-add", "Best album name");
        words.put("album-tracks-add", "Tracks in best album (number)");
        words.put("create-element", "Create element");

        // update screen
        words.put("update-title", "Editing an object");
        words.put("name-form-update", "Name");
        words.put("x-form-update", "X coordinate (number)");
        words.put("y-form-update", "Y coordinate (number)");
        words.put("number-of-participants-update", "Number of participants (number)");
        words.put("creation-date-update", "Establishment date");
        words.put("genre-update", "Genre");
        words.put("album-name-update", "Best album name");
        words.put("album-tracks-update", "Tracks in best album (number)");
        words.put("update-element", "Apply changes");
        words.put("remove-element", "Delete element");

        // errors
        words.put("empty-login", "Login can't be empty");
        words.put("empty-password", "Password can't be empty");
        words.put("password-not-match", "Passwords don't match");
        words.put("invalid-login", "Invalid login or password");
        words.put("user-exists", "Such a user already exists");

        words.put("empty-band-name", "Band name can't be empty");
        words.put("empty-x", "X coordinate must be a number");
        words.put("empty-y", "Y coordinate must be a number");
        words.put("empty-participants", "Number of participants must be a number");
        words.put("empty-date", "Establishment date must be a date");
        words.put("empty-genre", "Choose correct genre");
        words.put("empty-album", "Name of best album can't be empty");
        words.put("empty-tracks", "Number of tracks must be a number");

        words.put("add-error", "Element not added unknown error");
        words.put("owner-error", "You aren't the owner of this element");
        words.put("not-changed", "Make some changes");

        // header windows
        words.put("apply", "Apply");
        words.put("path", "Path to file");
        words.put("select", "Select");
        words.put("execute", "Execute");

        words.put("help", "Help");
        words.put("info", "Info");
        words.put("shuffle", "Shuffle");
        words.put("reorder", "Reorder");

        // help window
        words.put("help-title", "Help");
        words.put("info-title", "Info");
        words.put("shuffle-title", "Shuffle");
        words.put("reorder-title", "Reorder");

        words.put("help-info", "Show help information");
        words.put("info-info", "Show info about collection");
        words.put("shuffle-info", "Randomly shuffle collection");
        words.put("reorder-info", "Reorder collection");
        words.put("clear-info", "Clears the collection of your elements");
        words.put("add-info", "Add new element to collection(to delete or change an element, click on it in the table)");
        words.put("visual-info", "Go to visual display or go back to the table");

        // tableview columns
        words.put("filter", "Filter");
        words.put("clear", "Clear");
        words.put("cancel", "Cancel");

        words.put("id", "Id");
        words.put("name", "name");
        words.put("creation", "Creation date");
        words.put("participants", "Participants");
        words.put("estab-date", "Establishment date");
        words.put("genre", "Genre");
        words.put("album", "Album");
        words.put("tracks", "Tracks");
        words.put("user", "User");

        // toasts
        words.put("invalid-command", "Invalid command in the script");
        words.put("script-executed", "The script was executed");
        words.put("file-error", "The file was not found or there is no access to it");
        words.put("info-first", "Now there are ");
        words.put("info-second", " items in the collection");

        // genres
        words.put("Genre", "Genre");
        words.put("Hip hop", "Hip hop");
        words.put("Rap", "Rap");
        words.put("Post rock", "Post rock");
        words.put("Punk rock", "Punk rock");
        words.put("Brit pop", "Brit pop");

        // genres
        words.put("Genre-t", "Genre");
        words.put("Hip hop-t", "Hip hop");
        words.put("Rap-t", "Rap");
        words.put("Post rock-t", "Post rock");
        words.put("Punk rock-t", "Punk rock");
        words.put("Brit pop-t", "Brit pop");

    }

    @Override
    public String get(String s) {
        return words.get(s);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public String getName() {
        return name;
    }
}
