package org.slovenlypolygon.musicBandApp.client.ui.utils.filter;

import java.io.IOException;
import java.net.URL;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.stage.Popup;
import org.slovenlypolygon.musicBandApp.client.core.Client;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;

public final class FilterEventHandler<S> implements EventHandler<ActionEvent> {
    //	private static final String FILTER_DIALOG_FXML = "/filter/filter.fxml";
    private static final String FILTER_DIALOG_FXML = Client.class.getResource("/filter/filter.fxml").toString();
    //	private static final String FILTER_DIALOG_CSS = FilterEventHandler.class.getResource("filter-dialog.css").toString();
    private static final String FILTER_DIALOG_CSS = Client.class.getResource("/filter/filter-dialog.css").toString();

    private final TableColumn<S, String> tableColumn;
    private final Button filterButton;

    private Popup filterPopup;
    private FilterController<S> filterController;

    public FilterEventHandler(TableColumn<S, String> tableColumn, Button filterButton) {
        this.tableColumn = tableColumn;
        this.filterButton = filterButton;
    }

    @Override
    public void handle(ActionEvent event) {
        if (filterPopup == null) {
            filterPopup = createNewDialog();
        }

        Bounds bounds = filterButton.localToScreen(filterButton.getParent().getBoundsInParent());
        filterPopup.setX(bounds.getMinX());
        filterPopup.setY(bounds.getMaxY());
        filterPopup.setOnShowing(windowEvent -> {
            SceneProcessor.getInstance().getFilterController().redrawButtons();
            SceneProcessor.getInstance().switchTable();
        });
        filterPopup.setOnHiding(windowEvent -> SceneProcessor.getInstance().switchTable());
        filterPopup.setOnCloseRequest(windowEvent -> SceneProcessor.getInstance().switchTable());
        filterPopup.show(tableColumn.getTableView().getScene().getWindow());
        filterController.show();
    }

    private Popup createNewDialog() {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            URL resource = Client.class.getResource("/filter/filter.fxml");
            if (resource == null) {
                throw new RuntimeException("FXML file '" + FILTER_DIALOG_FXML + "' of the filter dialog not found!");
            }
            Parent filterDialogNode = fxmlLoader.load(Client.class.getResource("/filter/filter.fxml"));
            filterDialogNode.getStylesheets().add(FILTER_DIALOG_CSS);
            filterController = SceneProcessor.getInstance().getFilterController();
            filterController.setHandler(this);
            filterController.setColumn(tableColumn);

            // Add listener to inform FilterController if Table View data changed
            tableColumn.getTableView().itemsProperty().addListener((observable, oldValue, newValue) -> {
                filterController.tableDataChanged();
            });

            Popup popup = new Popup();
            popup.getScene().setRoot(filterDialogNode);
            popup.setAutoHide(true);
            return popup;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void setFilterActive(boolean filterActive) {
        if (filterActive && !filterButton.getStyleClass().contains("active")) {
            // Be sure to only add this once
            filterButton.getStyleClass().add("active");
        } else if (!filterActive && filterButton.getStyleClass().contains("active")) {
            filterButton.getStyleClass().remove("active");
        }
    }

    public void hidePopup() {
        filterPopup.hide();
    }

    public void redraw() {
        System.out.println("aboba");
    }
}