package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window;

import com.jfoenix.controls.JFXButton;
import javafx.animation.FadeTransition;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import javafx.util.StringConverter;
import org.slovenlypolygon.musicBandApp.client.core.helpers.FieldValidator;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestCreator;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestSender;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.ValidatingController;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;
import org.slovenlypolygon.musicBandApp.client.ui.utils.models.ErrorMessages;
import org.slovenlypolygon.musicBandApp.client.ui.utils.models.TableModel;
import org.slovenlypolygon.musicBandApp.common.models.MusicBand;
import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;
import org.slovenlypolygon.musicBandApp.common.utils.Mark;
import org.slovenlypolygon.musicBandApp.common.utils.Request;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class UpdateController extends ValidatingController {
    @FXML
    private AnchorPane updateWindow;
    @FXML
    private Pane backgroundPane;
    @FXML
    private Label updateTitle;
    @FXML
    private TextField nameUpdate;
    @FXML
    private TextField corXUpdate;
    @FXML
    private TextField corYUpdate;
    @FXML
    private TextField numOfParticipantsUpdate;
    @FXML
    private DatePicker estabDateUpdate;
    @FXML
    private ComboBox<MusicGenre> genreUpdate;
    @FXML
    private TextField albumNameUpdate;
    @FXML
    private TextField albumTracksUpdate;
    @FXML
    public Label incorrectPaneUpdate;
    @FXML
    private Button updateElementButton;
    @FXML
    private Button removeElementButton;
    @FXML
    private AnchorPane closeUpdateWindowButton;

    private final FadeTransition fadeInUpdateWindow = new FadeTransition(Duration.millis(300));
    private final FadeTransition fadeOutUpdateWindow = new FadeTransition(Duration.millis(300));

    private TableModel updatableElement;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setUpdateController(this);

        animationInitializing();
        handleEvents();
        formSetting();
        redrawButtons();
    }

    protected void handleEvents() {
        updateElementButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            tryToExecute(updatableElement);
        });

        updateElementButton.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ENTER) || keyEvent.getCode().equals(KeyCode.SPACE)) {
                tryToExecute(updatableElement);
            }
        });

        removeElementButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            tryToExecute(mouseEvent);
        });

        removeElementButton.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ENTER) || keyEvent.getCode().equals(KeyCode.SPACE)) {
                tryToExecute(keyEvent);
            }
        });

        closeUpdateWindowButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            fadeOutUpdateWindow.playFromStart();
            updateWindow.setDisable(true);
            updateWindow.setOpacity(0);

            clearWindow();
        });

        backgroundPane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeWindow();
        });
    }

    protected void tryToExecute(TableModel updatableElement) {
        MusicBand element = null;
        for (MusicBand musicBand : SceneProcessor.getInstance().getCollection()) {
            if (musicBand.getId() == Integer.parseInt(updatableElement.getId())) element = musicBand;
        }

        String name = this.nameUpdate.getText();
        String corX = this.corXUpdate.getText();
        String corY = this.corYUpdate.getText();
        String numOfParticipants = this.numOfParticipantsUpdate.getText();
        String estabDateText = this.estabDateUpdate.getValue().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));
        String genre = this.genreUpdate.getValue().toString();
        String albumName = this.albumNameUpdate.getText();
        String albumTracks = this.albumTracksUpdate.getText();

        System.out.println(genre);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale());
        ErrorMessages error = FieldValidator.getInstance().checkMusicBand(name, corX, corY, numOfParticipants, LocalDate.parse(estabDateText, formatter), genre, albumName, albumTracks);
        if (error == ErrorMessages.SUCCESS) {
            TableModel model = new TableModel(new JFXButton(), String.valueOf(element.getId()), name, corX, corY, element.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale())), numOfParticipants, estabDateText, SceneProcessor.getInstance().getLanguage().get(genre), albumName, albumTracks, element.getUser());
            if (model.equals(updatableElement)) error = ErrorMessages.NOT_CHANGED_ERROR;
        }
        if (error == ErrorMessages.SUCCESS) {
            error = validateAndGetRequest(name, corX, corY, numOfParticipants, LocalDate.parse(estabDateText, formatter), genre, albumName, albumTracks, element);
        }

        incorrectPaneUpdate.setText("  " + error.toString());

        if (error != ErrorMessages.SUCCESS) {
            fadeInIncorrectPane.playFromStart();
            System.out.println("Something was wrong");
        } else {
            SceneProcessor.getInstance().updateCollection();
            closeWindow();
        }
    }

    protected void tryToExecute(Event event) {
        MusicBand element = null;
        for (MusicBand musicBand : SceneProcessor.getInstance().getCollection()) {
            if (musicBand.getId() == Integer.parseInt(updatableElement.getId())) element = musicBand;
        }

        ErrorMessages error = validateAndGetRequest(element);
        incorrectPaneUpdate.setText("  " + error.toString());

        if (error != ErrorMessages.SUCCESS) {
            fadeInIncorrectPane.playFromStart();
            System.out.println("Something was wrong");
        } else {
            SceneProcessor.getInstance().updateCollection();
            closeWindow();
        }
    }

    private ErrorMessages validateAndGetRequest(String name, String corX, String corY, String numOfParticipants, LocalDate estabDateText, String genre, String albumName, String albumTracks, MusicBand element) {
        RequestSender sender = SceneProcessor.getInstance().getSender();
        ErrorMessages error = FieldValidator.getInstance().checkMusicBand(name, corX, corY, numOfParticipants, estabDateText, genre, albumName, albumTracks);

        if (error != ErrorMessages.SUCCESS) return error;

        Request request = RequestCreator.getInstance().updateRequest(name, corX, corY, numOfParticipants, estabDateText, genre, albumName, albumTracks, element, sender.getUser());
        boolean isUpdate = sender.send(request) == Mark.SUCCESS;
        return isUpdate ? ErrorMessages.SUCCESS : ErrorMessages.UPDATE_ERROR;
    }

    private ErrorMessages validateAndGetRequest(MusicBand element) {
        RequestSender sender = SceneProcessor.getInstance().getSender();
        Request request = RequestCreator.getInstance().removeRequest(element, sender.getUser());
        boolean isRemove = sender.send(request) == Mark.SUCCESS;
        return isRemove ? ErrorMessages.SUCCESS : ErrorMessages.UPDATE_ERROR;
    }

    public void closeWindow() {
        fadeOutUpdateWindow.playFromStart();
        updateWindow.setDisable(true);
        updateWindow.setOpacity(0);

        clearWindow();
    }

    public void openWindow(TableModel clickedRow) {
        this.updatableElement = clickedRow;
        fadeInUpdateWindow.playFromStart();
        updateWindow.setDisable(false);
        updateWindow.setOpacity(1);

        fillUpdateWindow();
    }

    private void clearWindow() {
        nameUpdate.clear();
        corXUpdate.clear();
        corYUpdate.clear();
        numOfParticipantsUpdate.clear();
        estabDateUpdate.getEditor().clear();
        genreUpdate.setValue(this.genreUpdate.getItems().get(0));
        albumNameUpdate.clear();
        albumTracksUpdate.clear();
    }


    private void fillUpdateWindow() {
        nameUpdate.setText(updatableElement.getName());
        corXUpdate.setText(String.valueOf(updatableElement.getCorX()));
        corYUpdate.setText(String.valueOf(updatableElement.getCorY()));
        numOfParticipantsUpdate.setText(String.valueOf(updatableElement.getNumberOfParticipants()));
        estabDateUpdate.setValue(updatableElement.getTrueEstabDate());
        estabDateUpdate.getEditor().setText(estabDateUpdate.getValue().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale())));
//        estabDateUpdate.getEditor().setText(estabDateUpdate.getValue().format(formatter));
        String genre = SceneProcessor.getInstance().getLanguage().get(updatableElement.getGenre() + "-t");
        if (genre == null) genre = updatableElement.getGenre();
        System.out.println(genre);
        genreUpdate.setValue(MusicGenre.getGenreByName(genre));
        albumNameUpdate.setText(updatableElement.getAlbumName());
        albumTracksUpdate.setText(String.valueOf(updatableElement.getAlbumTracks()));
    }

    private void formSetting() {
        genreSetting();
        this.genreUpdate.getItems().add(MusicGenre.GENRE);
        for (MusicGenre genre : MusicGenre.getAllGenres()) {
            this.genreUpdate.getItems().add(genre);
        }
        this.genreUpdate.setValue(this.genreUpdate.getItems().get(0));

        estabDateUpdate.setConverter(new StringConverter<>() {
            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null) return "";
                return localDate.format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));
            }

            @Override
            public LocalDate fromString(String s) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale());
                if (s == null || s.trim().isEmpty()) return null;
                return LocalDate.parse(s, formatter);
            }
        });
        estabDateUpdate.getEditor().setDisable(true);
        estabDateUpdate.setStyle("-fx-opacity: 1");
        estabDateUpdate.getEditor().setStyle("-fx-opacity: 1");
    }

    protected void animationInitializing() {
        fadeInIncorrectPane.setNode(incorrectPaneUpdate);
        fadeInIncorrectPane.setFromValue(0.0);
        fadeInIncorrectPane.setToValue(1.0);
        fadeInIncorrectPane.setCycleCount(1);
        fadeInIncorrectPane.setAutoReverse(false);

        fadeOutIncorrectPane.setNode(incorrectPaneUpdate);
        fadeOutIncorrectPane.setFromValue(1.0);
        fadeOutIncorrectPane.setToValue(0.0);
        fadeOutIncorrectPane.setCycleCount(1);
        fadeOutIncorrectPane.setAutoReverse(false);

        fadeInUpdateWindow.setNode(updateWindow);
        fadeInUpdateWindow.setFromValue(0.0);
        fadeInUpdateWindow.setToValue(1.0);
        fadeInUpdateWindow.setCycleCount(1);
        fadeInUpdateWindow.setAutoReverse(false);

        fadeOutUpdateWindow.setNode(updateWindow);
        fadeOutUpdateWindow.setFromValue(1.0);
        fadeOutUpdateWindow.setToValue(0.0);
        fadeOutUpdateWindow.setCycleCount(1);
        fadeOutUpdateWindow.setAutoReverse(false);
    }

    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();
        genreSetting();

        updateTitle.setText(language.get("update-title"));
        nameUpdate.setPromptText(language.get("name-form-update"));
        corXUpdate.setPromptText(language.get("x-form-update"));
        corYUpdate.setPromptText(language.get("y-form-update"));
        numOfParticipantsUpdate.setPromptText(language.get("number-of-participants-update"));
        estabDateUpdate.setPromptText(language.get("creation-date-update"));
        genreUpdate.getItems().set(0, MusicGenre.GENRE);
        albumNameUpdate.setPromptText(language.get("album-name-update"));
        albumTracksUpdate.setPromptText(language.get("album-tracks-update"));
        updateElementButton.setText(language.get("update-element"));
        removeElementButton.setText(language.get("remove-element"));
    }

    private void genreSetting() {
        this.genreUpdate.getItems().clear();
        for (MusicGenre genre : MusicGenre.getAllGenres()) {
            this.genreUpdate.getItems().add(genre);
        }
        this.genreUpdate.setValue(this.genreUpdate.getItems().get(0));

        genreUpdate.setConverter(new StringConverter<>() {
            @Override
            public String toString(MusicGenre musicGenre) {
                if (musicGenre == null) return "";
                return SceneProcessor.getInstance().getLanguage().get(musicGenre.toString());
            }

            @Override
            public MusicGenre fromString(String s) {
                for (MusicGenre genre: MusicGenre.getAllGenres()) {
                    if (genre.toString().equals(s)) return genre;
                }
                return null;
            }
        });
    }
}
