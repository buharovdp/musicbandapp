package org.slovenlypolygon.musicBandApp.client.ui.controllers;

public abstract class HeaderWindowController extends ValidatingController {
    public abstract void openWindow();
    public abstract void closeWindow();
}
