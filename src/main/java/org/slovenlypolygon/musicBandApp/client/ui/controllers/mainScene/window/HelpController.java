package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.HeaderWindowController;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;

import java.net.URL;
import java.util.ResourceBundle;

public class HelpController extends HeaderWindowController {
    @FXML
    private AnchorPane helpWindow;
    @FXML
    private Pane backgroundPane;
    @FXML
    private AnchorPane closeHelpWindowButton;
    @FXML
    private Label helpTitle;

    @FXML
    private Button help;
    @FXML
    private Button info;
    @FXML
    private Button shuffle;
    @FXML
    private Button reorder;
    @FXML
    private Button clear;
    @FXML
    private Button add;
    @FXML
    private Button goToVisual;

    @FXML
    private Label helpInfo;
    @FXML
    private Label infoInfo;
    @FXML
    private Label shuffleInfo;
    @FXML
    private Label reorderInfo;
    @FXML
    private Label clearInfo;
    @FXML
    private Label addInfo;
    @FXML
    private Label goToVisualInfo;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setHelpController(this);

        animationInitializing();
        handleEvents();
        redrawButtons();
    }

    @Override
    protected void handleEvents() {
        closeHelpWindowButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeWindow();
        });
        backgroundPane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent ->  {
            closeWindow();
        });
    }

    @Override
    protected void tryToExecute(Event event) {

    }

    @Override
    public void openWindow() {
        if (!helpWindow.isDisabled()) return;
        fadeInIncorrectPane.playFromStart();
        helpWindow.setDisable(false);
        helpWindow.setOpacity(1);
    }

    @Override
    public void closeWindow() {
        if (helpWindow.isDisabled()) return;
        fadeOutIncorrectPane.playFromStart();
        helpWindow.setDisable(true);
        helpWindow.setOpacity(0);
    }

    @Override
    protected void animationInitializing() {
        fadeInIncorrectPane.setNode(helpWindow);
        fadeInIncorrectPane.setFromValue(0.0);
        fadeInIncorrectPane.setToValue(1.0);
        fadeInIncorrectPane.setCycleCount(1);
        fadeInIncorrectPane.setAutoReverse(false);

        fadeOutIncorrectPane.setNode(helpWindow);
        fadeOutIncorrectPane.setFromValue(1.0);
        fadeOutIncorrectPane.setToValue(0.0);
        fadeOutIncorrectPane.setCycleCount(1);
        fadeOutIncorrectPane.setAutoReverse(false);
    }

    @Override
    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();

        helpTitle.setText(language.get("help-title"));

        help.setText(language.get("help-title"));
        helpInfo.setText(language.get("help-info"));
        info.setText(language.get("info-title"));
        infoInfo.setText(language.get("info-info"));
        shuffle.setText(language.get("shuffle-title"));
        shuffleInfo.setText(language.get("shuffle-info"));
        reorder.setText(language.get("reorder-title"));
        reorderInfo.setText(language.get("reorder-info"));

        clearInfo.setText(language.get("clear-info"));
        addInfo.setText(language.get("add-info"));
        goToVisualInfo.setText(language.get("visual-info"));
    }
}
