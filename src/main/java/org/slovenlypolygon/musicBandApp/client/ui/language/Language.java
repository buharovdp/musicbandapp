package org.slovenlypolygon.musicBandApp.client.ui.language;

import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public abstract class Language {
    private final Map<String, String> words = new HashMap<>();
    private final Locale locale = Locale.getDefault();
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final String name = "";

    public Language() {}

    public abstract String get(String s);

    public abstract Locale getLocale();

    public DateTimeFormatter getFormatter() {
        return formatter;
    }

    public String getName() {
        return name;
    }
}
