package org.slovenlypolygon.musicBandApp.client.ui.language;

import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Portuguese extends Language {
    private final Map<String, String> words = new HashMap<>();
    private final Locale locale = new Locale("pt", "PT");
    private final String name = "Portuguese";

    public Portuguese() {
        // signIn screen
        words.put("sign-in", "Entrar");
        words.put("login-form-in", "Sessao");
        words.put("password-form-in", "Senha");
        words.put("promote-text-in", "Não tem uma conta? ");
        words.put("sign-up-link", "Inscrever");

        // signUp screen
        words.put("sign-up-title", "Criar uma conta");
        words.put("sign-up", "Inscrever");
        words.put("login-form-up", "Sessao");
        words.put("password-form-up", "Senha");
        words.put("password-form-again-up", "Senha novamente");
        words.put("promote-text-up", "Já tem uma conta? ");
        words.put("sign-in-link", "Entrar");

        // main screen
        // header
        words.put("sign-out", "Sair");
        words.put("language", "Idioma");
        words.put("commands", "Comando");
        words.put("execute-script", "Executar script");

        // add screen
            words.put("add-title", "Adicionando um novo objeto");
        words.put("name-form-add", "Nome");
        words.put("x-form-add", "Coordenadas X (número)");
        words.put("y-form-add", "Coordenadas Y (número)");
        words.put("number-of-participants-add", "Número de participantes (número)");
            words.put("creation-date-add", "Data de estabelecimento");
            words.put("genre-add", "Genero");
        words.put("album-name-add", "Melhor nome do álbum");
        words.put("album-tracks-add", "Faixas em melhor álbum (número)");
        words.put("create-element", "Criar elemento");

        // update screen
        words.put("update-title", "Editar um objeto");
        words.put("name-form-update", "Nome");
        words.put("x-form-update", "Coordenadas X (número)");
        words.put("y-form-update", "Coordenadas Y (número)");
        words.put("number-of-participants-update", "Número de participantes (número)");
        words.put("creation-date-update", "Data de estabelecimento");
        words.put("genre-update", "Genero");
        words.put("album-name-update", "Melhor nome do álbum");
        words.put("album-tracks-update", "Faixas em melhor álbum (número)");
        words.put("update-element", "Aplicar alterações");
        words.put("remove-element", "Excluir elemento");

        // errors
        words.put("empty-login", "Login não pode estar vazio");
        words.put("empty-password", "A senha não pode estar vazia");
        words.put("password-not-match", "Senhas não correspondem");
        words.put("invalid-login", "Login ou senha inválidos");
        words.put("user-exists", "Esse usuário já existe");

        words.put("empty-band-name", "O nome da banda não pode estar vazio");
        words.put("empty-x", "A coordenada X deve ser um número");
        words.put("empty-y", "A coordenada Y deve ser um número");
        words.put("empty-participants", "O número de participantes deve ser um número");
        words.put("empty-date", "A data de estabelecimento deve ser uma data");
        words.put("empty-genre", "Escolha o gênero correto");
        words.put("empty-album", "Nome do melhor álbum não pode estar vazio");
        words.put("empty-tracks", "O número de faixas deve ser um número");

        words.put("add-error", "Elemento não adicionado erro desconhecido");
        words.put("owner-error", "Você não é o proprietário deste elemento");
        words.put("not-changed", "Faça algumas alterações");

        // header windows
        words.put("apply", "Aplicar");
        words.put("path", "Caminho para o arquivo");
        words.put("select", "Seleccionar");
        words.put("execute", "Executar");

        words.put("help", "Ajudar");
        words.put("info", "Info");
        words.put("shuffle", "Embaralhar");
        words.put("reorder", "Reordenar");

        // help window
        words.put("help-title", "Ajudar");
        words.put("info-title", "Info");
        words.put("shuffle-title", "Embaralhar");
        words.put("reorder-title", "Reordenar");

        words.put("help-info", "Mostrar informações de Ajuda");
        words.put("info-info", "Mostrar informações sobre collection");
        words.put("shuffle-info", "Aleatoriamente shuffle coleção");
        words.put("reorder-info", "Coleção reordenar");
        words.put("clear-info", "Limpa a coleção de seus elementos");
        words.put("add-info", "Adicionar novo elemento à coleção (para excluir ou alterar um elemento, clique nele na tabela)");
        words.put("visual-info", "Vá para exibição visual ou volte para a mesa");

        // tableview columns
        words.put("filter", "Filtro");
        words.put("clear", "Desmarcar");
        words.put("cancel", "Cancelar");

        words.put("id", "Id");
        words.put("name", "Nome");
        words.put("creation", "Creation date");
        words.put("participants", "Participants");
        words.put("estab-date", "Establishment date");
        words.put("genre", "Genre");
        words.put("album", "Album");
        words.put("tracks", "Tracks");
        words.put("user", "User");

        // toasts
        words.put("invalid-command", "Comando inválido no script");
        words.put("script-executed", "O script foi executado");
        words.put("file-error", "O arquivo não foi encontrado ou não há acesso a ele");
        words.put("info-first", "Agora existem ");
        words.put("info-second", " itens na coleção");

        // genres
        words.put("Genre", "Genero");
        words.put("Hip hop", "Hip hop");
        words.put("Rap", "Rap");
        words.put("Post rock", "Post rock");
        words.put("Punk rock", "Punk rock");
        words.put("Brit pop", "Brit pop");

        // genres
        words.put("Genero-t", "Genre");
        words.put("Hip hop-t", "Hip hop");
        words.put("Rap-t", "Rap");
        words.put("Post rock-t", "Post rock");
        words.put("Punk rock-t", "Punk rock");
        words.put("Brit pop-t", "Brit pop");

    }

    @Override
    public String get(String s) {
        return words.get(s);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public String getName() {
        return name;
    }
}
