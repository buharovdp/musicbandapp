package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.header;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.HeaderWindowController;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;

import java.net.URL;
import java.util.ResourceBundle;

public class CommandsController extends HeaderWindowController {
    @FXML
    private BorderPane commandsWindow;

    @FXML
    private Button helpCommandButton;
    @FXML
    private Button infoCommandButton;
    @FXML
    private Button shuffleCommandButton;
    @FXML
    private Button reorderCommandButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setCommandsController(this);

        animationInitializing();
        handleEvents();
        redrawButtons();
    }

    protected void handleEvents() {
        helpCommandButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            SceneProcessor.getInstance().getHelpController().openWindow();
            tryToExecute(mouseEvent);
        });

        infoCommandButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            Language language = SceneProcessor.getInstance().getLanguage();
            String info = language.get("info-first");
            info += String.valueOf(SceneProcessor.getInstance().getTableViewController().getSizeOfTable());
            info += language.get("info-second");
            SceneProcessor.getInstance().makeToast(info);
            tryToExecute(mouseEvent);
        });

        shuffleCommandButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            SceneProcessor.getInstance().getTableViewController().shuffle();
            tryToExecute(mouseEvent);
        });

        reorderCommandButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            SceneProcessor.getInstance().getTableViewController().reorder();
            tryToExecute(mouseEvent);
        });
    }

    @Override
    protected void tryToExecute(Event event) {
        closeWindow();
    }


    public void closeWindow() {
        if (commandsWindow.isDisabled()) return;
        fadeOutIncorrectPane.playFromStart();
        commandsWindow.setDisable(true);
        commandsWindow.setOpacity(0);
    }

    public void openWindow() {
        if (!commandsWindow.isDisabled()) return;
        fadeInIncorrectPane.playFromStart();
        commandsWindow.setDisable(false);
        commandsWindow.setOpacity(1);
    }

    @Override
    protected void animationInitializing() {
        fadeInIncorrectPane.setNode(commandsWindow);
        fadeInIncorrectPane.setFromValue(0.0);
        fadeInIncorrectPane.setToValue(1.0);
        fadeInIncorrectPane.setCycleCount(1);
        fadeInIncorrectPane.setAutoReverse(false);

        fadeOutIncorrectPane.setNode(commandsWindow);
        fadeOutIncorrectPane.setFromValue(1.0);
        fadeOutIncorrectPane.setToValue(0.0);
        fadeOutIncorrectPane.setCycleCount(1);
        fadeOutIncorrectPane.setAutoReverse(false);
    }

    @Override
    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();

        helpCommandButton.setText(language.get("help"));
        infoCommandButton.setText(language.get("info"));
        shuffleCommandButton.setText(language.get("shuffle"));
        reorderCommandButton.setText(language.get("reorder"));
    }
}
