package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.header;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import org.slovenlypolygon.musicBandApp.client.core.helpers.ScriptExecutor;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.HeaderWindowController;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;
import org.slovenlypolygon.musicBandApp.common.commands.Command;
import org.slovenlypolygon.musicBandApp.common.commands.exceptions.InvalidExecuteCommand;
import org.slovenlypolygon.musicBandApp.common.commands.inputSystem.CommandInput;
import org.slovenlypolygon.musicBandApp.common.commands.inputSystem.FileCommandInput;
import org.slovenlypolygon.musicBandApp.common.utils.Request;
import org.slovenlypolygon.musicBandApp.common.utils.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class ExecuteController extends HeaderWindowController {
    @FXML
    private BorderPane executeWindow;
    @FXML
    private TextField executeFilePath;
    @FXML
    private Button chooseFileButton;
    @FXML
    private Button executeScriptButton;

    private FileChooser fileChooser;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setExecuteController(this);
        animationInitializing();
        handleEvents();
        redrawButtons();

        fileChooserInitializing();
    }

    private void fileChooserInitializing() {
        fileChooser = new FileChooser();
        fileChooser.setTitle("Select Script");
        fileChooser.setInitialDirectory(new File("/."));
    }

    protected void handleEvents() {
        executeScriptButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            tryToExecute(mouseEvent);
        });
        chooseFileButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            File file = fileChooser.showOpenDialog(SceneProcessor.getInstance().getStage());
            if (file != null) {
                executeFilePath.setText(file.getAbsolutePath());
            }
        });
    }

    @Override
    protected void tryToExecute(Event event) {
        SceneProcessor.getInstance().getTableViewController().stopAllMusic();
        File file = new File(executeFilePath.getText());

        try {
            CommandInput reader = new FileCommandInput(file);
            while (true) {
                try {
                    Thread.sleep(100);
                    String line = reader.readLine();
                    if (line == null) break;

                    AbstractMap.SimpleEntry<Command, String> commandEntry = parseCommand(line);
                    Command command = commandEntry.getKey();
                    String commandArgs = commandEntry.getValue();

                    Request request = validateCommand(command, commandArgs, reader);
                    if (request == null) throw new Exception();

                    SceneProcessor.getInstance().getSender().send(request);
                } catch (Exception e) {
                    SceneProcessor.getInstance().makeToast(SceneProcessor.getInstance().getLanguage().get("invalid-command"));
                    return;
                }
            }
            SceneProcessor.getInstance().makeToast(SceneProcessor.getInstance().getLanguage().get("script-executed"));
        } catch (FileNotFoundException e) {
            SceneProcessor.getInstance().makeToast(SceneProcessor.getInstance().getLanguage().get("file-error"));
        }
        closeWindow();
    }


    public void closeWindow() {
        if (executeWindow.isDisabled()) return;
        fadeOutIncorrectPane.playFromStart();
        executeWindow.setDisable(true);
        executeWindow.setOpacity(0);
    }

    public void openWindow() {
        if (!executeWindow.isDisabled()) return;
        clearWindow();
        fadeInIncorrectPane.playFromStart();
        executeWindow.setDisable(false);
        executeWindow.setOpacity(1);
    }

    private void clearWindow() {
        executeFilePath.setText("");
        executeFilePath.setPromptText(SceneProcessor.getInstance().getLanguage().get("path"));
    }

    @Override
    protected void animationInitializing() {
        fadeInIncorrectPane.setNode(executeWindow);
        fadeInIncorrectPane.setFromValue(0.0);
        fadeInIncorrectPane.setToValue(1.0);
        fadeInIncorrectPane.setCycleCount(1);
        fadeInIncorrectPane.setAutoReverse(false);

        fadeOutIncorrectPane.setNode(executeWindow);
        fadeOutIncorrectPane.setFromValue(1.0);
        fadeOutIncorrectPane.setToValue(0.0);
        fadeOutIncorrectPane.setCycleCount(1);
        fadeOutIncorrectPane.setAutoReverse(false);
    }

    @Override
    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();

        executeFilePath.setPromptText(language.get("path"));
        chooseFileButton.setText(language.get("select"));
        executeScriptButton.setText(language.get("execute"));
    }

    private Request validateCommand(Command command, String commandArgs, CommandInput reader) {
        ScriptExecutor validator = new ScriptExecutor(reader);
        User user = SceneProcessor.getInstance().getUser();
        Request request = null;

        switch (command) {
            case HELP:
                request = validator.help(user);
                break;
            case INFO:
                request = validator.info(user);
                break;
            case SHOW:
                request = validator.show(user);
                break;
            case ADD:
                request = validator.add(user);
                break;
            case UPDATE_ID:
                request = validator.updateId(commandArgs, user);
                break;
            case REMOVE_BY_ID:
                request = validator.removeId(commandArgs, user);
                break;
            case CLEAR:
                request = validator.clear(user);
                break;
            case EXIT:
                validator.exit();
                break;
            case SHUFFLE:
                request = validator.shuffle(user);
                break;
            case SORT:
                request = validator.sort(user);
                break;
            case REORDER:
                request = validator.reorder(user);
                break;
            case SHOW_BY_ALBUM:
                request = validator.showByBestAlbum(user);
                break;
            case SHOW_GREATER_THAN_ALBUM:
                request = validator.showGreaterThanAlbum(user);
                break;
            case SHOW_NUM_OF_PARTICIPANTS:
                request = validator.showNumberOfParticipants(user);
                break;
        }
        return request;
    }

    private AbstractMap.SimpleEntry<Command, String> parseCommand(String stringCommand) {
        if (stringCommand.split(" ").length > 0) {
            String commandArgs;
            List<String> commandArr = Arrays.asList(stringCommand.split(" "));
            Command command = Command.getCommandByName(commandArr.get(0));
            if (commandArr.size() > 1) commandArgs = commandArr.get(1);
            else commandArgs = "";

            if (command != null) return new AbstractMap.SimpleEntry<>(command, commandArgs);
        }

        throw new InvalidExecuteCommand();
    }
}
