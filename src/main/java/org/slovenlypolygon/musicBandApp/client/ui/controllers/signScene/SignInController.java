package org.slovenlypolygon.musicBandApp.client.ui.controllers.signScene;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.slovenlypolygon.musicBandApp.client.core.helpers.FieldValidator;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestCreator;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestSender;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.ValidatingController;
import org.slovenlypolygon.musicBandApp.client.ui.utils.models.ErrorMessages;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;
import org.slovenlypolygon.musicBandApp.common.utils.Mark;
import org.slovenlypolygon.musicBandApp.common.utils.Request;

import java.net.URL;
import java.util.ResourceBundle;

public class SignInController extends ValidatingController {
    @FXML
    private AnchorPane parentContainer;
    @FXML
    private AnchorPane container;
    @FXML
    private Label signInTitle;
    @FXML
    private Button signInButton;
    @FXML
    private Label signUpLink;
    @FXML
    private TextField loginForm;
    @FXML
    private TextField passwordForm;
    @FXML
    private Label incorrectPane;
    @FXML
    private  Label promptText;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        redrawButtons();
        handleEvents();
        animationInitializing();
    }

    protected void handleEvents() {
        signUpLink.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            loadSignUpScreen(mouseEvent);
        });

        signInButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            tryToExecute(mouseEvent);
        });
        signInButton.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ENTER) || keyEvent.getCode().equals(KeyCode.SPACE)) {
                tryToExecute(keyEvent);
            }
        });

        SceneProcessor.getInstance().getStage().widthProperty().addListener((obs, oldVal, newVal) -> {
            parentContainer.setPrefWidth(newVal.doubleValue());
        });

        SceneProcessor.getInstance().getStage().heightProperty().addListener((obs, oldVal, newVal) -> {
            parentContainer.setPrefHeight(newVal.doubleValue() - 32);
        });

        loginForm.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (incorrectPane.getOpacity() == 1) fadeOutIncorrectPane.playFromStart();
        });
        passwordForm.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (incorrectPane.getOpacity() == 1) fadeOutIncorrectPane.playFromStart();
        });
    }

    protected void tryToExecute(Event event) {
        ErrorMessages error = validateAndGetRequest(loginForm.getText().toLowerCase(), passwordForm.getText());
        incorrectPane.setText("  " + error.toString());

        if (error != ErrorMessages.SUCCESS) {
            fadeInIncorrectPane.playFromStart();
            System.out.println("Something was wrong");
        } else {
            SceneProcessor.getInstance().openMainScene(event, parentContainer, container);
        }
    }

    private ErrorMessages validateAndGetRequest(String login, String password) {
        RequestSender sender = SceneProcessor.getInstance().getSender();
        ErrorMessages error = FieldValidator.getInstance().checkSignInForm(login, password);
        if (error != ErrorMessages.SUCCESS) return error;

        Request request = RequestCreator.getInstance().signInRequest(login, password, sender.getUser());
        boolean isLogin = sender.send(request) == Mark.USER;
        return isLogin ? ErrorMessages.SUCCESS : ErrorMessages.NO_USER;
    }

    protected void animationInitializing() {
        fadeInIncorrectPane.setNode(incorrectPane);
        fadeInIncorrectPane.setFromValue(0.0);
        fadeInIncorrectPane.setToValue(1.0);
        fadeInIncorrectPane.setCycleCount(1);
        fadeInIncorrectPane.setAutoReverse(false);

        fadeOutIncorrectPane.setNode(incorrectPane);
        fadeOutIncorrectPane.setFromValue(1.0);
        fadeOutIncorrectPane.setToValue(0.0);
        fadeOutIncorrectPane.setCycleCount(1);
        fadeOutIncorrectPane.setAutoReverse(false);
    }

    private void loadSignUpScreen(Event event) {
        SceneProcessor.getInstance().openSignUpScene(event, parentContainer, container);
    }

    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();

        signInTitle.setText(language.get("sign-in"));
        loginForm.setPromptText(language.get("login-form-in"));
        passwordForm.setPromptText(language.get("password-form-in"));
        signInButton.setText(language.get("sign-in"));
        promptText.setText(language.get("promote-text-in"));
        signUpLink.setText(language.get("sign-up-link"));
    }
}