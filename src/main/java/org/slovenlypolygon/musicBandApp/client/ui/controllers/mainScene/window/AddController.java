package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window;

import javafx.animation.FadeTransition;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.Duration;
import javafx.util.StringConverter;
import org.slovenlypolygon.musicBandApp.client.core.helpers.FieldValidator;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestCreator;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestSender;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.ValidatingController;
import org.slovenlypolygon.musicBandApp.client.ui.utils.models.ErrorMessages;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;
import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;
import org.slovenlypolygon.musicBandApp.common.utils.Mark;
import org.slovenlypolygon.musicBandApp.common.utils.Request;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class AddController extends ValidatingController {
    @FXML
    private AnchorPane addWindow;
    @FXML
    private Pane backgroundPane;
    @FXML
    private Label addTitle;
    @FXML
    private TextField nameAdd;
    @FXML
    private TextField corXAdd;
    @FXML
    private TextField corYAdd;
    @FXML
    private TextField numOfParticipantsAdd;
    @FXML
    private DatePicker estabDateAdd;
    @FXML
    private ComboBox<MusicGenre> genreAdd;
    @FXML
    private TextField albumNameAdd;
    @FXML
    private TextField albumTracksAdd;
    @FXML
    public Label incorrectPaneAdd;
    @FXML
    private Button createElementButton;
    @FXML
    private AnchorPane closeAddWindowButton;

    private final FadeTransition fadeInAddWindow = new FadeTransition(Duration.millis(300));
    private final FadeTransition fadeOutAddWindow = new FadeTransition(Duration.millis(300));

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setAddController(this);

        animationInitializing();
        handleEvents();
        formSetting();
        redrawButtons();
    }

    protected void handleEvents() {
        createElementButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            tryToExecute(mouseEvent);
        });

        createElementButton.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ENTER) || keyEvent.getCode().equals(KeyCode.SPACE)) {
                tryToExecute(keyEvent);
            }
        });

        closeAddWindowButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeWindow();
        });

        backgroundPane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeWindow();
        });
    }

    protected void tryToExecute(Event event) {
        String name = nameAdd.getText();
        String corX = corXAdd.getText();
        String corY = corYAdd.getText();
        String numOfParticipants = numOfParticipantsAdd.getText();
        LocalDate estabDateText = estabDateAdd.getValue();
        String genre = genreAdd.getValue().toString();
        String albumName = albumNameAdd.getText();
        String albumTracks = albumTracksAdd.getText();

        ErrorMessages error = validateAndGetRequest(name, corX, corY, numOfParticipants, estabDateText, genre, albumName, albumTracks);
        incorrectPaneAdd.setText("  " + error.toString());

        if (error != ErrorMessages.SUCCESS) {
            fadeInIncorrectPane.playFromStart();
            System.out.println("Something was wrong");
        } else {
            SceneProcessor.getInstance().updateCollection();
            closeWindow();
        }
    }

    private ErrorMessages validateAndGetRequest(String name, String corX, String corY, String numOfParticipants, LocalDate estabDateText, String genre, String albumName, String albumTracks) {
        RequestSender sender = SceneProcessor.getInstance().getSender();
        ErrorMessages error = FieldValidator.getInstance().checkMusicBand(name, corX, corY, numOfParticipants, estabDateText, genre, albumName, albumTracks);

        if (error != ErrorMessages.SUCCESS) return error;

        Request request = RequestCreator.getInstance().addRequest(name, corX, corY, numOfParticipants, estabDateText, genre, albumName, albumTracks, sender.getUser());
        boolean isAdd = sender.send(request) == Mark.SUCCESS;
        return isAdd ? ErrorMessages.SUCCESS : ErrorMessages.ADD_ERROR;
    }

    public void closeWindow() {
        fadeOutAddWindow.playFromStart();
        addWindow.setDisable(true);
        addWindow.setOpacity(0);

        clearWindow();
    }

    public void openWindow() {
        fadeInAddWindow.playFromStart();
        addWindow.setDisable(false);
        addWindow.setOpacity(1);

        clearWindow();
    }

    private void clearWindow() {
        nameAdd.clear();
        corXAdd.clear();
        corYAdd.clear();
        numOfParticipantsAdd.clear();
        estabDateAdd.getEditor().clear();
        estabDateAdd.setValue(null);
        genreAdd.setValue(this.genreAdd.getItems().get(0));
        albumNameAdd.clear();
        albumTracksAdd.clear();
    }

    private void formSetting() {
        genreSetting();

        estabDateAdd.setConverter(new StringConverter<>() {
            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null) return "";
                return localDate.format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));
            }

            @Override
            public LocalDate fromString(String s) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale());
                if (s == null || s.trim().isEmpty()) return null;
                return LocalDate.parse(s, formatter);
            }
        });
        estabDateAdd.getEditor().setDisable(true);
        estabDateAdd.setStyle("-fx-opacity: 1");
        estabDateAdd.getEditor().setStyle("-fx-opacity: 1");
    }

    private void genreSetting() {
        genreAdd.getItems().clear();
        for (MusicGenre genre : MusicGenre.getAllGenres()) {
            this.genreAdd.getItems().add(genre);
        }
        this.genreAdd.setValue(this.genreAdd.getItems().get(0));

        genreAdd.setConverter(new StringConverter<>() {
            @Override
            public String toString(MusicGenre musicGenre) {
                if (musicGenre == null) return "";
                return SceneProcessor.getInstance().getLanguage().get(musicGenre.toString());
            }

            @Override
            public MusicGenre fromString(String s) {
                for (MusicGenre genre: MusicGenre.getAllGenres()) {
                    if (genre.toString().equals(s)) return genre;
                }
                return null;
            }
        });
    }

    protected void animationInitializing() {
        fadeInIncorrectPane.setNode(incorrectPaneAdd);
        fadeInIncorrectPane.setFromValue(0.0);
        fadeInIncorrectPane.setToValue(1.0);
        fadeInIncorrectPane.setCycleCount(1);
        fadeInIncorrectPane.setAutoReverse(false);

        fadeOutIncorrectPane.setNode(incorrectPaneAdd);
        fadeOutIncorrectPane.setFromValue(1.0);
        fadeOutIncorrectPane.setToValue(0.0);
        fadeOutIncorrectPane.setCycleCount(1);
        fadeOutIncorrectPane.setAutoReverse(false);

        fadeInAddWindow.setNode(addWindow);
        fadeInAddWindow.setFromValue(0.0);
        fadeInAddWindow.setToValue(1.0);
        fadeInAddWindow.setCycleCount(1);
        fadeInAddWindow.setAutoReverse(false);

        fadeOutAddWindow.setNode(addWindow);
        fadeOutAddWindow.setFromValue(1.0);
        fadeOutAddWindow.setToValue(0.0);
        fadeOutAddWindow.setCycleCount(1);
        fadeOutAddWindow.setAutoReverse(false);
    }

    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();
        genreSetting();

        addTitle.setText(language.get("add-title"));
        nameAdd.setPromptText(language.get("name-form-add"));
        corXAdd.setPromptText(language.get("x-form-add"));
        corYAdd.setPromptText(language.get("y-form-add"));
        numOfParticipantsAdd.setPromptText(language.get("number-of-participants-add"));
        estabDateAdd.setPromptText(language.get("creation-date-add"));
        genreAdd.getItems().set(0, MusicGenre.GENRE);
        albumNameAdd.setPromptText(language.get("album-name-add"));
        albumTracksAdd.setPromptText(language.get("album-tracks-add"));
        createElementButton.setText(language.get("create-element"));
    }
}
