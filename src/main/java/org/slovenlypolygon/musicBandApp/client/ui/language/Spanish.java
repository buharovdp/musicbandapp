package org.slovenlypolygon.musicBandApp.client.ui.language;

import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Spanish extends Language {
    private final Map<String, String> words = new HashMap<>();
    private final Locale locale = new Locale("es", "EC");
    private final String name = "Spanish";

    public Spanish() {
        // signIn screen
        words.put("sign-in", "Iniciar Sesión");
        words.put("login-form-in", "Sesión");
        words.put("password-form-in", "Contraseña");
        words.put("promote-text-in", "No tienes una cuenta? ");
        words.put("sign-up-link", "Inscribir");

        // signUp screen
        words.put("sign-up-title", "Crear una cuenta");
        words.put("sign-up", "Inscribir");
        words.put("login-form-up", "Sesión");
        words.put("password-form-up", "Contraseña");
        words.put("password-form-again-up", "Contraseña de nuevo");
        words.put("promote-text-up", "Ya tienes una cuenta? ");
        words.put("sign-in-link", "Iniciar Sesión");

        // main screen
        // header
        words.put("sign-out", "Cerrar Sesión");
        words.put("language", "Idioma");
        words.put("commands", "Comando");
        words.put("execute-script", "Ejecutar script");

        // add screen
        words.put("add-title", "Agregar un objeto nuevo");
        words.put("name-form-add", "Nombre");
        words.put("x-form-add", "Coordenada X (número)");
        words.put("y-form-add", "Coordenada Y (número)");
        words.put("number-of-participants-add", "Número de participantes (número)");
        words.put("creation-date-add", "Fecha de establecimiento");
        words.put("genre-add", "Género");
        words.put("album-name-add", "Mejor álbum");
        words.put("album-tracks-add", "Canciones en mejor álbum (número)");
        words.put("create-element", "Crear elemento");

        // update screen
        words.put("update-title", "Edición de un objeto");
        words.put("name-form-update", "Nombre");
        words.put("x-form-update", "Coordenada X (número)");
        words.put("y-form-update", "Coordenada Y (número)");
        words.put("number-of-participants-update", "Número de participantes (número)");
        words.put("creation-date-update", "Fecha de establecimiento");
        words.put("genre-update", "Género");
        words.put("album-name-update", "Mejor álbum");
        words.put("album-tracks-update", "Canciones en mejor álbum (número)");
        words.put("update-element", "Aplicar cambios");
        words.put("remove-element", "Eliminar elemento");

        // errors
        words.put("empty-login", "El inicio de sesión no puede estar vacío");
        words.put("empty-password", "La contraseña no puede estar vacía");
        words.put("password-not-match", "Las contraseñas no coinciden");
        words.put("invalid-login", "Nombre de usuario o contraseña no válidos");
        words.put("user-exists", "Dicho usuario ya existe");

        words.put("empty-band-name", "El nombre de la banda no puede estar vacío");
        words.put("empty-x", "La coordenada X debe ser un número");
        words.put("empty-y", "La coordenada Y debe ser un número");
        words.put("empty-participants", "Número de participantes debe ser un número");
        words.put("empty-date", "La fecha de establecimiento debe ser una fecha");
        words.put("empty-genre", "Elige el género correcto");
        words.put("empty-album", "El nombre del mejor álbum no puede estar vacío");
        words.put("empty-tracks", "Número de pistas debe ser un número");

        words.put("add-error", "Elemento no añadido error desconocido");
        words.put("owner-error", "No eres el propietario de este elemento");
        words.put("not-changed", "Hacer algunos cambios");

        // header windows
        words.put("apply", "Aplicar");
        words.put("path", "Ruta al archivo");
        words.put("select", "Seleccionar");
        words.put("execute", "Ejecutar");

        words.put("help", "Ayudar");
        words.put("info", "Info");
        words.put("shuffle", "Barajar");
        words.put("reorder", "Reordenar");

        // help window
        words.put("help-title", "Ayudar");
        words.put("info-title", "Info");
        words.put("shuffle-title", "Barajar");
        words.put("reorder-title", "Reordenar");

        words.put("help-info", "Mostrar información de ayuda");
        words.put("info-info", "Mostrar información sobre la colección");
        words.put("shuffle-info", "Colección aleatoria aleatoria");
        words.put("reorder-info", "Reordenar colección");
        words.put("clear-info", "Borra la colección de elementos");
        words.put("add-info", "Agregar nuevo elemento a la colección(para eliminar o cambiar un elemento, haga clic en él en la tabla)");
        words.put("visual-info", "Ir a la presentación visual o volver a la mesa");

        // tableview columns
        words.put("filter", "Filtro");
        words.put("clear", "Claro");
        words.put("cancel", "Cancelar");

        words.put("id", "Id");
        words.put("name", "Nombre");
        words.put("creation", "Fecha de creación");
        words.put("participants", "Participante");
        words.put("estab-date", "Fecha de establecimiento");
        words.put("genre", "Género");
        words.put("album", "Album");
        words.put("tracks", "Pista");
        words.put("user", "Usuario");

        // toasts
        words.put("invalid-command", "Comando no válido en el script");
        words.put("script-executed", "Se ejecutó el script");
        words.put("file-error", "No se encontró el archivo o no hay acceso a él");
        words.put("info-first", "Ahora hay ");
        words.put("info-second", "artículos de la colección");

        // genres
        words.put("Genre", "Género");
        words.put("Hip hop", "Hip hop");
        words.put("Rap", "Rap");
        words.put("Post rock", "Post rock");
        words.put("Punk rock", "Punk rock");
        words.put("Brit pop", "Brit pop");

        // genres
        words.put("Género-t", "Genre");
        words.put("Hip hop-t", "Hip hop");
        words.put("Rap-t", "Rap");
        words.put("Post rock-t", "Post rock");
        words.put("Punk rock-t", "Punk rock");
        words.put("Brit pop-t", "Brit pop");

    }

    @Override
    public String get(String s) {
        return words.get(s);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public String getName() {
        return name;
    }
}
