package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene;

import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.utils.filter.FilterSupport;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;
import org.slovenlypolygon.musicBandApp.client.ui.utils.models.TableModel;
import org.slovenlypolygon.musicBandApp.common.models.MusicBand;

import java.net.URL;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.Stack;

public class TableViewController implements Initializable {
    @FXML
    private TableView<TableModel> table;
    @FXML
    private TableColumn<TableModel, String> playCol;
    @FXML
    private TableColumn<?, String> idCol;
    @FXML
    private TableColumn<?, String> nameCol;
    @FXML
    private TableColumn<?, String> corXCol;
    @FXML
    private TableColumn<?, String> corYCol;
    @FXML
    private TableColumn<?, String> creationDateCol;
    @FXML
    private TableColumn<?, String> participantsCol;
    @FXML
    private TableColumn<?, String> estabDateCol;
    @FXML
    private TableColumn<?, String> genreCol;
    @FXML
    private TableColumn<?, String> albumNameCol;
    @FXML
    private TableColumn<?, String> tracksCol;
    @FXML
    private TableColumn<?, String> userCol;

    private final ObservableList<TableModel> tableCollection = FXCollections.observableArrayList();
    private final boolean setWrappableLabels = false;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SceneProcessor.getInstance().setTableViewController(this);

        updateTable();
        loadDate();
        addFilters();

        for (TableColumn<?, ?> column : table.getColumns()) {
            column.setResizable(true);
            column.setEditable(false);
            column.setReorderable(false);
        }


//        playCol.setReorderable(false);
//        playCol.setResizable(false);
    }

    private void addFilters() {
        FilterSupport.addFilter(idCol, setWrappableLabels);
        FilterSupport.addFilter(nameCol, setWrappableLabels);
        FilterSupport.addFilter(corXCol, setWrappableLabels);
        FilterSupport.addFilter(corYCol, setWrappableLabels);
        FilterSupport.addFilter(creationDateCol, setWrappableLabels);
        FilterSupport.addFilter(participantsCol, setWrappableLabels);
        FilterSupport.addFilter(estabDateCol, setWrappableLabels);
        FilterSupport.addFilter(genreCol, setWrappableLabels);
        FilterSupport.addFilter(albumNameCol, setWrappableLabels);
        FilterSupport.addFilter(tracksCol, setWrappableLabels);
        FilterSupport.addFilter(userCol, setWrappableLabels);
    }

    private void loadDate() {
        playCol.setCellValueFactory(new PropertyValueFactory<>("button"));
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        corXCol.setCellValueFactory(new PropertyValueFactory<>("corX"));
        corYCol.setCellValueFactory(new PropertyValueFactory<>("corY"));
        creationDateCol.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
        participantsCol.setCellValueFactory(new PropertyValueFactory<>("numberOfParticipants"));
        estabDateCol.setCellValueFactory(new PropertyValueFactory<>("estabDate"));
        genreCol.setCellValueFactory(new PropertyValueFactory<>("genre"));
        albumNameCol.setCellValueFactory(new PropertyValueFactory<>("albumName"));
        tracksCol.setCellValueFactory(new PropertyValueFactory<>("albumTracks"));
        userCol.setCellValueFactory(new PropertyValueFactory<>("userName"));

        table.setItems(tableCollection);
    }

    public void updateTable() {
        tableCollection.clear();
        generateTableCollection();
//        loadDate();

    }

    private void generateTableCollection() {
        String id;
        String name;
        String corX;
        String corY;
        String creationDate;
        String numberOfParticipants;
        String estabDate;
        String genre;
        String albumName;
        String albumTracks;
        String userName;

        for (MusicBand musicBand : SceneProcessor.getInstance().getCollection()) {
            id = String.valueOf(musicBand.getId());
            name = musicBand.getName();
            corX = String.valueOf(musicBand.getCoordinates().getX());
            corY = String.valueOf(musicBand.getCoordinates().getY());
            creationDate = musicBand.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));
            numberOfParticipants = String.valueOf(musicBand.getNumberOfParticipants());
//            estabDate = musicBand.getEstablishmentDate().toString();
            estabDate = musicBand.getEstablishmentDate().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));
            genre = SceneProcessor.getInstance().getLanguage().get(musicBand.getGenre().toString());
            albumName = musicBand.getBestAlbum().getName();
            albumTracks = String.valueOf(musicBand.getBestAlbum().getTracks());
            userName = musicBand.getUser();

            tableCollection.add(new TableModel(new JFXButton(), id, name, corX, corY, creationDate, numberOfParticipants, estabDate, genre, albumName, albumTracks, userName));
        }
    }

    public void redrawButtons() {
        Language language = SceneProcessor.getInstance().getLanguage();

        Stack<MusicBand> col = SceneProcessor.getInstance().getCollection();
        String id;
        String name;
        String corX;
        String corY;
        String creationDate;
        String numberOfParticipants;
        String estabDate;
        String genre;
        String albumName;
        String albumTracks;
        String userName;
        for (int i = 0; i < tableCollection.size(); i++) {
            id = String.valueOf(col.get(i).getId());
            name = col.get(i).getName();
            corX = String.valueOf(col.get(i).getCoordinates().getX());
            corY = String.valueOf(col.get(i).getCoordinates().getY());
            creationDate = col.get(i).getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));
            numberOfParticipants = String.valueOf(col.get(i).getNumberOfParticipants());
//            estabDate = musicBand.getEstablishmentDate().toString();
            estabDate = col.get(i).getEstablishmentDate().format(DateTimeFormatter.ofPattern("d-MMM-yyyy", SceneProcessor.getInstance().getLanguage().getLocale()));
            genre = SceneProcessor.getInstance().getLanguage().get(col.get(i).getGenre().toString());
            albumName = col.get(i).getBestAlbum().getName();
            albumTracks = String.valueOf(col.get(i).getBestAlbum().getTracks());
            userName = col.get(i).getUser();

            tableCollection.set(i, new TableModel(new JFXButton(), id, name, corX, corY, creationDate, numberOfParticipants, estabDate, genre, albumName, albumTracks, userName));
        }

        table.refresh();

        if (!setWrappableLabels) {
            idCol.setText(language.get("id"));
            nameCol.setText(language.get("name"));
            corXCol.setText("X");
            corYCol.setText("Y");
            creationDateCol.setText(language.get("creation"));
            participantsCol.setText(language.get("participants"));
            estabDateCol.setText(language.get("estab-date"));
            genreCol.setText(language.get("genre"));
            albumNameCol.setText(language.get("album"));
            tracksCol.setText(language.get("tracks"));
            userCol.setText(language.get("user"));
        }
    }

    public void stopAllMusic() {
        for (TableModel model : tableCollection) {
            model.setPLaying(false, model.getName(), model.getAlbumName());
        }
    }

    public void switchDisabledTable() {
        table.setMouseTransparent(!table.isMouseTransparent());
    }

    public int getSizeOfTable() {
        return tableCollection.size();
    }

    public void shuffle() {
        FXCollections.shuffle(tableCollection);
        table.refresh();
    }

    public void reorder() {
        FXCollections.reverse(tableCollection);
        table.refresh();
    }
}
