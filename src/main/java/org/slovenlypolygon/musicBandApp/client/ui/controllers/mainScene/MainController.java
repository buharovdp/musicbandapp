package org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestCreator;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.HeaderWindowController;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window.AddController;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window.UpdateController;
import org.slovenlypolygon.musicBandApp.client.ui.utils.filter.FilterController;
import org.slovenlypolygon.musicBandApp.client.ui.utils.models.TableModel;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;
import org.slovenlypolygon.musicBandApp.common.utils.Request;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    private AnchorPane parentContainer;
    @FXML
    private AnchorPane container;

    @FXML
    private Button signOutButton;
    @FXML
    private Button languageButton;
    @FXML
    private Button commandsButton;
    @FXML
    private Button executeButton;

    @FXML
    private Label userName;
    @FXML
    private Button addButton;
    @FXML
    private Button clearButton;
    @FXML
    private Button visualButton;

    @FXML
    private TableView<TableModel> table;
    @FXML
    AnchorPane addWindowXML;
    @FXML
    AnchorPane updateWindowXML;
    @FXML
    BorderPane languageWindow;
    @FXML
    BorderPane commandsWindow;
    @FXML
    BorderPane executeWindow;

    private AddController addController;
    private UpdateController updateController;
    private HeaderWindowController languageController;
    private HeaderWindowController commandsController;
    private HeaderWindowController executeController;
    private HeaderWindowController helpController;
    private TableViewController tableViewController;
    private FilterController filterController;

    private SceneProcessor sceneProcessor;
    private TableModel clickedRow;
    private List<HeaderWindowController> headerControllerList = new ArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        sceneProcessor = SceneProcessor.getInstance();
        redrawButtons();
        initializeControllers();

        handleEvents();
        setUsername();
    }

    private void initializeControllers() {
        addController = sceneProcessor.getAddController();
        updateController = sceneProcessor.getUpdateController();
        languageController = sceneProcessor.getLanguageController();
        commandsController = sceneProcessor.getCommandsController();
        executeController = sceneProcessor.getExecuteController();
        helpController = sceneProcessor.getHelpController();
        tableViewController = sceneProcessor.getTableViewController();
        filterController = sceneProcessor.getFilterController();

        headerControllerList.add(languageController);
        headerControllerList.add(commandsController);
        headerControllerList.add(executeController);
        headerControllerList.add(helpController);
    }

    public void setUsername() {
        String login = sceneProcessor.getUser().getLogin();
        if (login == null) login = "not user";
        login = login.substring(0, 1).toUpperCase() + login.substring(1).toLowerCase();
        userName.setText(login);
    }

    private void handleEvents() {
        sceneProcessor.getStage().widthProperty().addListener((obs, oldVal, newVal) -> {
            parentContainer.setPrefWidth(newVal.doubleValue());
//            executeWindow.setLayoutX(executeWindow.getLayoutX() + (newVal.doubleValue() - oldVal.doubleValue()));
//            commandsWindow.setLayoutX(commandsWindow.getLayoutX() + (newVal.doubleValue() - oldVal.doubleValue()));
        });

        sceneProcessor.getStage().heightProperty().addListener((obs, oldVal, newVal) -> {
            parentContainer.setPrefHeight(newVal.doubleValue() - 32);
        });

        signOutButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeAnotherWindows(null);
            if (tableViewController != null) tableViewController.stopAllMusic();
            sceneProcessor.setCollection(null);
            sceneProcessor.openSignInScene(mouseEvent, parentContainer, container);
        });

        addButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeAnotherWindows(null);
            addController.openWindow();
        });
        
        clearButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (tableViewController != null) tableViewController.stopAllMusic();
            Request clearRequest = RequestCreator.getInstance().clearRequest(sceneProcessor.getUser());
            sceneProcessor.getSender().send(clearRequest);
            sceneProcessor.updateCollection();
        });

        visualButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            closeAnotherWindows(null);
            if (tableViewController != null) tableViewController.stopAllMusic();
            sceneProcessor.openVisualScene(mouseEvent, parentContainer, container);
        });

        languageButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (languageWindow.isDisabled()) {
                closeAnotherWindows(languageController);
                languageController.openWindow();
            } else languageController.closeWindow();
        });

        commandsButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (commandsWindow.isDisabled()) {
                closeAnotherWindows(commandsController);
                commandsController.openWindow();
            } else commandsController.closeWindow();
        });

        executeButton.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (executeWindow.isDisabled()) {
                closeAnotherWindows(executeController);
                executeController.openWindow();
            }
            else executeController.closeWindow();
        });

        // set event handler for all rows in table
        table.setRowFactory(tableModelTableView -> {
            TableRow<TableModel> row = new TableRow<>();
            row.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
                clickedRow = row.getItem();
                closeAnotherWindows(null);
                if (!row.isEmpty()) updateController.openWindow(clickedRow);
            });
            return row;
        });
    }

    private void closeAnotherWindows(HeaderWindowController controller) {
        if (tableViewController != null) tableViewController.stopAllMusic();
        for (HeaderWindowController c : headerControllerList) {
            if (controller != c) c.closeWindow();
        }
    }

    public void redrawButtons() {
        Language language = sceneProcessor.getLanguage();
        signOutButton.setText(language.get("sign-out"));
        languageButton.setText(language.get("language"));
        commandsButton.setText(language.get("commands"));
        executeButton.setText(language.get("execute-script"));

        if (addController != null) addController.redrawButtons();
        if (updateController != null) updateController.redrawButtons();
        if (filterController != null) filterController.redrawButtons();
        if (tableViewController != null) tableViewController.redrawButtons();

        for (HeaderWindowController controller: headerControllerList) {
            if (controller != null) controller.redrawButtons();
        }
    }
}
