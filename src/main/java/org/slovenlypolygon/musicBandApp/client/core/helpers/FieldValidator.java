package org.slovenlypolygon.musicBandApp.client.core.helpers;

import org.slovenlypolygon.musicBandApp.client.ui.utils.models.ErrorMessages;
import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;

import java.time.LocalDate;

public class FieldValidator {
    private static FieldValidator validator;

    private FieldValidator() {
    }

    public ErrorMessages checkSignInForm(String login, String password) {
        ErrorMessages error = ErrorMessages.SUCCESS;

        if (login.trim().isEmpty()) error = ErrorMessages.LOGIN;
        else if (password.trim().isEmpty()) error = ErrorMessages.PASSWORD;

        return error;
    }

    public ErrorMessages checkSignUpForm(String login, String password, String passwordAgain) {
        ErrorMessages error = ErrorMessages.SUCCESS;

        if (login.trim().isEmpty()) error = ErrorMessages.LOGIN;
        else if (!password.equals(passwordAgain)) error = ErrorMessages.REPEAT_PASSWORD;
        else if (password.trim().isEmpty()) error = ErrorMessages.PASSWORD;

        return error;
    }

    public ErrorMessages checkMusicBand(String name, String x, String y, String participants, LocalDate estabDate, String gen, String albumName, String tracks) {
        if (isNullString(name)) return ErrorMessages.BAND_NAME;
        else if (!isNumber(x)) return ErrorMessages.COR_X;
        else if (!isNumber(y)) return ErrorMessages.COR_Y;
        else if (!isNumber(participants)) return ErrorMessages.PARTICIPANTS;
        else if (Integer.parseInt(participants) <= 0) return ErrorMessages.PARTICIPANTS;
        else if (estabDate == null || estabDate.toString().trim().isEmpty()) return ErrorMessages.ESTAB_DATE;
        else if (MusicGenre.getGenreByName(gen) == null || MusicGenre.getGenreByName(gen) == MusicGenre.GENRE) return ErrorMessages.GENRE;
        else if (isNullString(albumName)) return ErrorMessages.BEST_ALBUM;
        else if (!isNumber(tracks)) return ErrorMessages.TRACKS;
        else if (Integer.parseInt(tracks) <= 0) return ErrorMessages.TRACKS;
        else return ErrorMessages.SUCCESS;
    }

    private boolean isNullString(String string) {
        return string.trim().isEmpty();
    }

    private boolean isNumber(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static FieldValidator getInstance() {
        if (validator == null) validator = new FieldValidator();
        return validator;
    }
}
