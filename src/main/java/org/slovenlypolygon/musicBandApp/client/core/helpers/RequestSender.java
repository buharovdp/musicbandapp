package org.slovenlypolygon.musicBandApp.client.core.helpers;

import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import org.slovenlypolygon.musicBandApp.common.commands.Command;
import org.slovenlypolygon.musicBandApp.common.utils.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.time.Instant;

public class RequestSender implements Runnable {
    private final DatagramChannel channel;
    private final SocketAddress serverAddress;

    private volatile User user;


    public RequestSender(SocketAddress socketAddress, DatagramChannel channel, User user) {
        this.serverAddress = socketAddress;
        this.channel = channel;
        this.user = user;
    }

    public synchronized Object send(Request request) {
        ByteArrayOutputStream serializedRequest = Serializator.serialize(request);
        Response responseFormServer = sendRequestAndGetResponse(channel, serializedRequest, serverAddress);
        return processResponse(responseFormServer);
    }

    private Response sendRequestAndGetResponse(DatagramChannel client, ByteArrayOutputStream message, SocketAddress serverAddress) {
        Response responseFromServer = null;
        try {
            // Create buffer with request
            ByteBuffer request = ByteBuffer.wrap(message.toByteArray());
            ByteBuffer serverResponse = ByteBuffer.allocate(Serializator.MAX_SIZE);

            // Try to send request and get response
            outer:
            for (int i = 0; i < 5; i++) {
                Instant deadline = Instant.now().plusSeconds(1);
                while (Instant.now().isBefore(deadline)) {
                    request.rewind();
                    int numSent = client.send(request, serverAddress);
                    if (numSent > 0) break;
                    Thread.sleep(100);
                }

                deadline = Instant.now().plusSeconds(1);
                while (Instant.now().isBefore(deadline)) {
                    SocketAddress address = client.receive(serverResponse);
                    if (serverAddress.equals(address)) break outer;
                    Thread.sleep(100);
                }
            }

            // Serialize response
            responseFromServer = (Response) Serializator.deserialize(serverResponse.array());

            // Confirm response (sending confirm request)
            ByteBuffer requestWithConfirm = ByteBuffer.wrap(Serializator.serialize(new Request(null, responseFromServer.getIdentificationString(), user)).toByteArray());
            client.send(requestWithConfirm, serverAddress);
        } catch (Exception e) {
            System.err.println("Ошибка при отправке пакета на сервер :(");
        }
        return responseFromServer;
    }

    private Object processResponse(Response responseFromServer) {
        String title = responseFromServer.getTitle();
        Object message = responseFromServer.getMessage();
        Mark mark = responseFromServer.getMark();

//        if (!title.isEmpty()) System.out.println(title);

        if (mark == Mark.USER) user = (User) message;
        if (mark == Mark.COLLECTION) return message;
        if (mark == Mark.UPDATE) return true;
        if (mark == Mark.NOT_UPDATE) return false;
        return mark;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(3000);
                if (user == null) continue;
                Object answer = send(new Request(Command.IS_UPDATED, SceneProcessor.getInstance().getCollection(), user));
                boolean isUpdated = (boolean) answer;
                if (isUpdated) {
                    System.out.println("Collection was updated");
                    new Thread(() -> {
                        SceneProcessor.getInstance().updateCollection();
                    }).start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void closeConnection() {
        try {
            channel.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
