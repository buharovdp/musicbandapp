package org.slovenlypolygon.musicBandApp.client.core.helpers;

import org.slovenlypolygon.musicBandApp.common.commands.Command;
import org.slovenlypolygon.musicBandApp.common.commands.inputSystem.CommandInput;
import org.slovenlypolygon.musicBandApp.common.models.*;
import org.slovenlypolygon.musicBandApp.common.utils.Request;
import org.slovenlypolygon.musicBandApp.common.utils.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Pattern;


/**
 * ЧЕЛ, КОТОРЫЙ УМЕЕТ ИСПОЛНЯТЬ КОМАНДЫ И ДРУЖИТ С КОЛЛЕКЦИЕЙ
 */
public class ScriptExecutor {
    private final CommandInput reader;

    /**
     * @param reader ЧИТАТЕЛЬ ЛИБО КОНСОЛЬНЫЙ, ЛИБО ФАЙЛОВЫЙ
     */
    public ScriptExecutor(CommandInput reader) {
        this.reader = reader;
    }


    public Request log(User user) {
        System.out.println("Вход в аккаунт...");

        System.out.println("Введите логин:");
        String login = inputNonNullString();
        System.out.println("Введите пароль:");
        String password = null;
        try {
            password = encryptPassword(inputNonNullString());
        } catch (NoSuchAlgorithmException ignored) {
        }

        User newUser = new User();
        newUser.setLogin(login);
        newUser.setPassword(password);
        return new Request(Command.LOG, newUser, user);
    }

    public Request reg(User user) {
        System.out.println("Регистрация нового аккаунта");

        System.out.println("Введите логин:");
        String login = inputNonNullString();
        String password;

        while (true) {
            System.out.println("Введите пароль:");
            password = inputNonNullString();
            System.out.println("Повторите пароль:");

            if (!password.equals(inputNonNullString())) System.out.println("Пароли не совпадают, повторите снова");
            else break;
        }

        try {
            password = encryptPassword(password);
        } catch (NoSuchAlgorithmException ignored) {
        }

        User newUser = new User();
        newUser.setLogin(login);
        newUser.setPassword(password);
        return new Request(Command.REG, newUser, user);
    }

    public String encryptPassword(final String password) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
        messageDigest.update(password.getBytes());
        byte byteBuffer[] = messageDigest.digest();
        StringBuffer strHexString = new StringBuffer();

        for (int i = 0; i < byteBuffer.length; i++) {
            String hex = Integer.toHexString(0xff & byteBuffer[i]);
            if (hex.length() == 1) {
                strHexString.append('0');
            }
            strHexString.append(hex);
        }
        return strHexString.toString();
    }

    /**
     * СПРАВКА
     */
    public Request help(User user) {
        return new Request(Command.HELP, null, user);
    }

    /**
     * ВЫВОДИТ ИНФУ О КОЛЛЕКЦИИ
     *
     * @param user
     */
    public Request info(User user) {
        return new Request(Command.INFO, null, user);
    }

    /**
     * ПОКАЗЫВАЕТ КОЛЛЕКЦИЮ
     *
     * @param user
     */
    public Request show(User user) {
        return new Request(Command.SHOW, null, user);
    }


    /**
     * ДОБАВЛЯЕТ НОВЫЙ ЭЛЕМЕНТ В КОЛЛЕКЦИЮ
     *
     * @param user
     */
    public Request add(User user) {
        System.out.println("Добавление нового элемента в коллекцию:");
        MusicBand element = createMusicBand(user.getLogin());
        if (element != null) return new Request(Command.ADD, element, user);
        else return null;
    }

    /**
     * МЕНЯЕТ ДАННЫЙ ЭЛЕМЕНТ
     *
     * @param commandArgs - АЙДИ ЭЛЕМЕНТА
     * @param user
     */
    public Request updateId(String commandArgs, User user) {
        try {
            int id = Integer.parseInt(commandArgs);
            System.out.println("Изменение элемента коллекции:");
            return new Request(Command.UPDATE_ID, new AbstractMap.SimpleEntry<>(id, createMusicBand(user.getLogin())), user);
        } catch (NumberFormatException e) {
            System.out.println("Аргумент должен быть числом, возврат в главное меню...");
            return null;
        }
    }

    /**
     * УДАЛЯЕТ ЭЛЕМЕНТ ПО ДАННОМУ АЙДИ
     *
     * @param commandArgs АЙДИ ЭЛЕМЕНТА
     * @param user
     */
    public Request removeId(String commandArgs, User user) {
        try {
            int id = Integer.parseInt(commandArgs);
            return new Request(Command.REMOVE_BY_ID, id, user);
        } catch (NumberFormatException e) {
            System.out.println("Аргумент должен быть числом, возврат в главное меню...");
            return null;
        }
    }

    /**
     * ОЧИЩАЕТ КОЛЛЕКЦИЮ
     *
     * @param user
     */
    public Request clear(User user) {
        return new Request(Command.CLEAR, null, user);
    }

    /**
     * ВЫХОДИТ ИЗ ПОРОГРАММЫ
     */
    public void exit() {
        System.out.println("Закрытие программы...");
        System.exit(0);
    }

    /**
     * МЕШАЕТ КОЛЛЕКЦИЮ
     *
     * @param user
     */
    public Request shuffle(User user) {
        return new Request(Command.SHUFFLE, null, user);
    }

    /**
     * СОРТИРУЕТ КОЛЛЕКЦИЮ ПО УМОЛЧАНИЮ
     *
     * @param user
     */
    public Request sort(User user) {
        return new Request(Command.SORT, null, user);
    }

    /**
     * РАЗВОРАЧИВАЕТ КОЛЛЕКЦИЮ
     *
     * @param user
     */
    public Request reorder(User user) {
        return new Request(Command.REORDER, null, user);
    }

    /**
     * ПОКАЗЫВАЕТ СОВПАДЕНИЯ С ДАННЫМ АЛЬБОМОМ
     *
     * @param user
     */
    public Request showByBestAlbum(User user) {
        System.out.println("Введите название альбома, по которому будет произведен фильтр:");
        String albumName = inputNonNullString();
        if (albumName == null) return null;
        return new Request(Command.SHOW_BY_ALBUM, albumName, user);
    }

    /**
     * ПОКАЗЫВАЕТ САМЫЕ КРУТЫЕ АЛЬБОМЫ
     *
     * @param user
     */
    public Request showGreaterThanAlbum(User user) {
        System.out.println("Введите название альбома, по которому будет произведен фильтр:");
        String albumName = inputNonNullString();
        System.out.println("Введите количество треков в альбоме");
        int tracks = (int) inputPositiveLong();
        Album album = new Album(albumName, tracks);
        if (albumName == null || tracks == Integer.MIN_VALUE) return null;
        return new Request(Command.SHOW_GREATER_THAN_ALBUM, album, user);
    }

    /**
     * КОМАНДА, ПО ВЫВОДУ ОТСОРТИРОВАННЫХ УЧАСТНИКОВ ГРУППЫ
     *
     * @param user
     */
    public Request showNumberOfParticipants(User user) {
        return new Request(Command.SHOW_NUM_OF_PARTICIPANTS, null, user);
    }


    /**
     * ПРОВЕРЯЕТ КОРРЕКТНОСТЬ СТРОКИ
     *
     * @return ВОЗВРАЩАЕТ СТРОКУ
     */
    private String inputNonNullString() {
        String input = reader.readLine();
        if (!input.equals("")) return input;
        return null;
    }

    /**
     * ПРОВЕРЯЕТ КОРРЕКТНОСТЬ ЧИСЛА
     *
     * @return ВОЗВРАЩАЕТ ЧИСЛО
     */
    private int inputInt() {
        try {
            return Integer.parseInt(reader.readLine());
        } catch (NumberFormatException e) {
            return -Integer.MIN_VALUE;
        }
    }

    /**
     * ПРОВЕРЯЕТ КОРРЕКТНОСТЬ ЧИСЛА
     *
     * @return ВОЗВРАЩАЕТ ЧИСЛО
     */
    private long inputPositiveLong() {
        try {
            long input = Long.parseLong(reader.readLine());
            if (input > 0) return input;
        } catch (NumberFormatException e) {
            return Integer.MIN_VALUE;
        }
        return Integer.MIN_VALUE;
    }


    /**
     * ПРОВЕРЯЕТ КОРРЕКТНОСТЬ ДАТЫ
     *
     * @return ВОЗВРАЩАЕТ ДАТУ
     */
    private String inputDate() {
        String input = reader.readLine();
        if (Pattern.matches("^-?\\+?(\\d{1,4})[-](\\+?0[1-9]|\\+?1[0-2])[-](\\+?[0-2]?[1-9]|\\+?[1-3][0-1])$", input)) {
            try {
                LocalDate.parse(input);
                return input;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    /**
     * ПРОВЕРЯЕТ КОРРЕКТНОСТЬ ЖАНРА
     *
     * @return ВОЗВРАЩАЕТ ЖАНР
     */
    private MusicGenre inputGenre() {
        MusicGenre input = MusicGenre.getGenreByName(reader.readLine());
        return input;
    }

    /**
     * СОЗДАЕТ НОВУЮ МУЗЫЧКУ
     *
     * @return ВОЗВРАЩАЕТ НОВУЮ МУЗЫЧКУ
     */
    private MusicBand createMusicBand(String user) {
        // name of album
        System.out.println("Введите имя альбома");
        String name = inputNonNullString();
        if (name == null) return null;

        // coordinates
        System.out.println("Введите значение координаты по oX:");
        int corX = inputInt();
        if (corX == Integer.MIN_VALUE) return null;
        System.out.println("Введите значение координаты по oY:");
        int corY = inputInt();
        if (corY == Integer.MIN_VALUE) return null;
        Coordinates coordinates = new Coordinates(corX, corY);

        // number of participants
        System.out.println("Введите количество участников:");
        long numberOfParticipants = inputPositiveLong();
        if (numberOfParticipants == Integer.MIN_VALUE) return null;
        // date
        System.out.println("Введите дату создания группы в формате yyyy-mm-dd:");
        String dateText = inputDate();
        if (dateText == null) return null;
        LocalDate date = LocalDate.parse(dateText);


        // music genre
        System.out.println("Выберите жанр: ");
        System.out.println("Жанры на выбор:");
        MusicGenre.getAllGenres().forEach(System.out::println);
        System.out.println("Введите нужный жанр:");
        MusicGenre genre = inputGenre();
        if (genre == null) return null;

        // best album
        System.out.println("Введите название лучшего альбома группы:");
        String albumName = inputNonNullString();
        if (albumName == null) return null;
        System.out.println("Введите количество треков в альбоме:");
        int tracks = (int) inputPositiveLong();
        if (tracks == Integer.MIN_VALUE) return null;
        Album album = new Album(albumName, tracks);

        return new MusicBand(-1, name, coordinates, new Date(), numberOfParticipants, date, genre, album, user);
    }

    public CommandInput getReader() {
        return reader;
    }
}

