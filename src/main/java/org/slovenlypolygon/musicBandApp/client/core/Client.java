package org.slovenlypolygon.musicBandApp.client.core;

import javafx.application.Application;
import javafx.stage.Stage;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestSender;
import org.slovenlypolygon.musicBandApp.common.utils.Serializator;
import org.slovenlypolygon.musicBandApp.common.utils.User;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.DatagramChannel;

public class Client extends Application {
    private static RequestSender sender;

    private static final String clientId = "312d9bb28cd0401cb6b8ce99184bd1ec";
    private static final String clientSecret = "9df112a7db964f9c9fa4076bd2e32630";


    public static void main(String[] args) {
        try {
            // Connect to channel
            DatagramChannel channel = DatagramChannelBuilder.bindChannel(null);
            channel.configureBlocking(false);

            // Connect to server
            InetSocketAddress serverAddress = new InetSocketAddress("localhost", Serializator.PORT);
            sender = new RequestSender(serverAddress, channel, new User());

            launch();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void start(Stage stage) {
        try {
            SceneProcessor.getInstance().start(stage, sender);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
