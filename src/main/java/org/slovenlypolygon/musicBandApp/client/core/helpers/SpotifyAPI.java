package org.slovenlypolygon.musicBandApp.client.core.helpers;

import com.neovisionaries.i18n.CountryCode;
import javafx.concurrent.Task;
import org.apache.hc.core5.http.ParseException;
import org.slovenlypolygon.musicBandApp.client.core.SceneProcessor;
import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.exceptions.SpotifyWebApiException;
import se.michaelthelin.spotify.model_objects.credentials.ClientCredentials;
import se.michaelthelin.spotify.model_objects.specification.*;
import se.michaelthelin.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import se.michaelthelin.spotify.requests.data.albums.GetAlbumsTracksRequest;
import se.michaelthelin.spotify.requests.data.artists.GetArtistsAlbumsRequest;
import se.michaelthelin.spotify.requests.data.search.simplified.SearchArtistsRequest;
import se.michaelthelin.spotify.requests.data.tracks.GetTrackRequest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SpotifyAPI {
    private static final String clientId = "312d9bb28cd0401cb6b8ce99184bd1ec";
    private static final String clientSecret = "9df112a7db964f9c9fa4076bd2e32630";
    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setClientId(clientId)
            .setClientSecret(clientSecret)
            .build();

    public SpotifyAPI() {
        try {
            ClientCredentialsRequest clientCredentialsRequest = spotifyApi.clientCredentials().build();

            ClientCredentials clientCredentials = clientCredentialsRequest.execute();
            System.out.println("CLIENT CREDENTIALS: " + clientCredentials);

            spotifyApi.setAccessToken(clientCredentials.getAccessToken());
            System.out.println("ACCESS TOKEN: " + spotifyApi);

        } catch (IOException | SpotifyWebApiException | ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public String findMusic(String albumName, String bandName) {
        try {
            Track musicPreviewTrack;

            musicPreviewTrack = findTrack(albumName, bandName);

            if (musicPreviewTrack == null) {
                createTask("Track wasn't found, check band name and album name please, playing '1000-7'");
                musicPreviewTrack = findTrack("1000-7", "fem.love");
            }
            if (musicPreviewTrack.getPreviewUrl() == null) {
                System.out.println("WITHOUT PREVIEW");
                System.out.println("TRACK ID: " + musicPreviewTrack.getId());

                createTask("The track was found, but it is impossible to listen to it outside of spotify, , playing '1000-7'");
                musicPreviewTrack = findTrack("1000-7", "fem.love");
            }

            String url = musicPreviewTrack.getPreviewUrl();
            System.out.println("TRACK URL: " + url);
            return url;
        } catch (IOException | ParseException | SpotifyWebApiException e) {
            throw new RuntimeException(e);
        }
    }

    private void createTask(String msg) {
        Task task = new Task<Void>() {
            @Override
            public Void call() {
                SceneProcessor.getInstance().makeToast(msg);
                return null;
            }
        };
        task.setOnSucceeded(workerStateEvent -> {
            SceneProcessor.getInstance().makeToast(msg);
        });
        task.setOnFailed(workerStateEvent -> {
            SceneProcessor.getInstance().makeToast(msg);
        });
        new Thread(task).start();
    }


    private Track findTrack(String albumName, String bandName) throws IOException, ParseException, SpotifyWebApiException {
        SearchArtistsRequest searchArtistsRequest = spotifyApi.searchArtists(bandName).build();
        Paging<Artist> artistPaging = searchArtistsRequest.execute();
        Artist[] artists = artistPaging.getItems();

        for (Artist artist : artists) {
            if (artist.getName().toLowerCase().contains(bandName.toLowerCase()))
                return getArtistsAlbums(artist.getId(), albumName);
        }

        return null;
    }

    private Track getArtistsAlbums(String id, String albumName) throws IOException, ParseException, SpotifyWebApiException {
        GetArtistsAlbumsRequest getArtistsAlbumsRequest = spotifyApi.getArtistsAlbums(id).market(CountryCode.SE).build();
        Paging<AlbumSimplified> albumSimplifiedPaging = getArtistsAlbumsRequest.execute();
        List<AlbumSimplified> albums = Arrays.asList(albumSimplifiedPaging.getItems());

        for (AlbumSimplified album : albums) {
            if (album.getName().toLowerCase().contains(albumName.toLowerCase())) {
                return getTrackByAlbumId(album.getId());
            }
        }

        return getTrackByAlbumId(albums.get(0).getId());
    }

    private Track getTrackByAlbumId(String id) throws IOException, ParseException, SpotifyWebApiException {
        GetAlbumsTracksRequest getAlbumsTracksRequest = spotifyApi.getAlbumsTracks(id).market(CountryCode.SE).build();
        Paging<TrackSimplified> trackSimplifiedPaging = getAlbumsTracksRequest.execute();

        TrackSimplified[] tracks = trackSimplifiedPaging.getItems();

        for (TrackSimplified track : tracks) {
            if (track.getPreviewUrl() != null) {
                GetTrackRequest getTrackRequest = spotifyApi.getTrack(track.getId()).market(CountryCode.SE).build();
                return getTrackRequest.execute();
            }
        }

        return spotifyApi.getTrack(tracks[0].getId()).market(CountryCode.SE).build().execute();
    }
}