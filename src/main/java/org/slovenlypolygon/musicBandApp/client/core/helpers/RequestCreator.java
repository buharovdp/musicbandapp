package org.slovenlypolygon.musicBandApp.client.core.helpers;

import org.slovenlypolygon.musicBandApp.common.commands.Command;
import org.slovenlypolygon.musicBandApp.common.models.Album;
import org.slovenlypolygon.musicBandApp.common.models.Coordinates;
import org.slovenlypolygon.musicBandApp.common.models.MusicBand;
import org.slovenlypolygon.musicBandApp.common.models.MusicGenre;
import org.slovenlypolygon.musicBandApp.common.utils.Request;
import org.slovenlypolygon.musicBandApp.common.utils.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.Date;

public class RequestCreator {
    private static RequestCreator creator;

    private RequestCreator() {}

    public Request signInRequest(String login, String password, User user) {
        return new Request(Command.LOG, new User(login, encryptPassword(password)), user);
    }

    public Request signUpRequest(String login, String password, User user) {
        return new Request(Command.REG, new User(login, encryptPassword(password)), user);
    }

    public Request getCollectionRequest(User user) {
        return new Request(Command.SHOW, null, user);
    }

    public Request addRequest(String name, String corX, String corY, String numOfParticipants, LocalDate estabDate, String genre, String albumName, String albumTracks, User user) {
        int x = Integer.parseInt(corX);
        int y = Integer.parseInt(corY);
        int participants = Integer.parseInt(numOfParticipants);
        MusicGenre musicGenre = MusicGenre.getGenreByName(genre);
        int tracks = Integer.parseInt(albumTracks);

        return new Request(Command.ADD, new MusicBand(-1, name, new Coordinates(x, y), new Date(), participants, estabDate, musicGenre, new Album(albumName, tracks), user.getLogin()), user);
    }

    public Request clearRequest(User user) {
        return new Request(Command.CLEAR, null, user);
    }

    public Request updateRequest(String name, String corX, String corY, String numOfParticipants, LocalDate estabDate, String genre, String albumName, String albumTracks, MusicBand element, User user) {
        int x = Integer.parseInt(corX);
        int y = Integer.parseInt(corY);
        int participants = Integer.parseInt(numOfParticipants);
        MusicGenre musicGenre = MusicGenre.getGenreByName(genre);
        int tracks = Integer.parseInt(albumTracks);

        MusicBand musicBand = new MusicBand(element.getId(), name, new Coordinates(x, y), element.getCreationDate(), participants, estabDate, musicGenre, new Album(albumName, tracks), element.getUser());
        return new Request(Command.UPDATE_ID, new AbstractMap.SimpleEntry<>(element.getId(), musicBand), user);
    }

    public Request removeRequest(MusicBand element, User user) {
        return new Request(Command.REMOVE_BY_ID, element.getId(), user);
    }

    public static RequestCreator getInstance() {
        if (creator == null) creator = new RequestCreator();
        return creator;
    }

    private String encryptPassword(final String password) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        messageDigest.update(password.getBytes());
        byte byteBuffer[] = messageDigest.digest();
        StringBuffer strHexString = new StringBuffer();

        for (int i = 0; i < byteBuffer.length; i++) {
            String hex = Integer.toHexString(0xff & byteBuffer[i]);
            if (hex.length() == 1) {
                strHexString.append('0');
            }
            strHexString.append(hex);
        }
        return strHexString.toString();
    }
}
