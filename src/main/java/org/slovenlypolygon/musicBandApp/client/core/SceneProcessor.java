package org.slovenlypolygon.musicBandApp.client.core;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.slovenlypolygon.musicBandApp.client.core.helpers.SpotifyAPI;
import org.slovenlypolygon.musicBandApp.client.ui.Toast;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.HeaderWindowController;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.*;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestCreator;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.header.ExecuteController;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.header.LanguageController;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window.AddController;
import org.slovenlypolygon.musicBandApp.client.ui.controllers.mainScene.window.UpdateController;
import org.slovenlypolygon.musicBandApp.client.ui.utils.filter.FilterController;
import org.slovenlypolygon.musicBandApp.client.ui.language.English;
import org.slovenlypolygon.musicBandApp.client.ui.language.Language;
import org.slovenlypolygon.musicBandApp.common.models.MusicBand;
import org.slovenlypolygon.musicBandApp.client.core.helpers.RequestSender;
import org.slovenlypolygon.musicBandApp.common.utils.User;

import java.io.IOException;
import java.util.Objects;
import java.util.Stack;

public class SceneProcessor {
    private static final SpotifyAPI spotifyAPI = new SpotifyAPI();

    private static SceneProcessor instance;
    private static Scene currentScene;
    private static Pane root;
    private static Stage stage;

    private RequestSender sender;
    private Stack<MusicBand> collection;
    private Language language;

    private MainController mainController;
    private AddController addController;
    private UpdateController updateController;
    private HeaderWindowController languageController;
    private HeaderWindowController commandsController;
    private HeaderWindowController executeController;
    private HeaderWindowController helpController;
    private TableViewController tableViewController;

    private CanvasController canvasController;
    private VisualController visualController;

    private FilterController<?> filterController;

    private SceneProcessor() {
    }

    public void start(Stage stage, RequestSender sender) throws IOException {
        SceneProcessor.stage = stage;
        setLanguage(new English());
        this.sender = sender;

        FXMLLoader loader = new FXMLLoader();
        root = loader.load(Objects.requireNonNull(Client.class.getResource("/layout/sign/sign-in.fxml")));
        mainController = loader.getController();

        currentScene = new Scene(root);
        currentScene.getStylesheets().add(Objects.requireNonNull(Client.class.getResource("/css/main.css")).toExternalForm());
        currentScene.setFill(Color.TRANSPARENT);

        SceneProcessor.stage.setTitle("MusicBandApp");
        SceneProcessor.stage.setScene(currentScene);
        SceneProcessor.stage.setMinHeight(800);
        SceneProcessor.stage.setMinWidth(1024);
        SceneProcessor.stage.setResizable(true);

        root.setPrefWidth(stage.getWidth());
        root.setPrefHeight(stage.getHeight() - 32);
        SceneProcessor.stage.show();

        SceneProcessor.stage.setOnCloseRequest(windowEvent -> {
            sender.closeConnection();
            System.exit(0);
        });
    }

    public static SceneProcessor getInstance() {
        if (instance == null) instance = new SceneProcessor();
        return instance;
    }

    public void openSignInScene(Event event, AnchorPane parentContainer, AnchorPane container) {
        try {
            sender.setUser(null);
            root = FXMLLoader.load(Objects.requireNonNull(Client.class.getResource("/layout/sign/sign-in.fxml")));
            currentScene = ((Node) event.getSource()).getScene();
            playSlideAnimation(parentContainer, container, -1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void openSignUpScene(Event event, AnchorPane parentContainer, AnchorPane container) {
        try {
            root = FXMLLoader.load(Objects.requireNonNull(Client.class.getResource("/layout/sign/sign-up.fxml")));
            currentScene = ((Node) event.getSource()).getScene();
            playSlideAnimation(parentContainer, container, 1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void openMainScene(Event event, AnchorPane parentContainer, AnchorPane container) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Objects.requireNonNull(Client.class.getResource("/layout/main/main.fxml")));
            root = loader.load();
            mainController = loader.getController();

            currentScene = ((Node) event.getSource()).getScene();
            playSlideAnimation(parentContainer, container, 1);

            // start thread with updating collection
            Thread updateCollection = new Thread(sender);
            updateCollection.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void openVisualScene(Event event, AnchorPane parentContainer, AnchorPane container) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Objects.requireNonNull(Client.class.getResource("/layout/visual.fxml")));
            root = loader.load();

            currentScene = ((Node) event.getSource()).getScene();
            playSlideAnimation(parentContainer, container, 1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateCollection() {
        this.collection = (Stack<MusicBand>) sender.send(RequestCreator.getInstance().getCollectionRequest(sender.getUser()));
        if (tableViewController != null) tableViewController.updateTable();

        Task task = new Task<Void>() {
            @Override
            public Void call() {
                if (canvasController != null) {canvasController.updateCanvas();}
                if (visualController != null) {visualController.setEventHandler();}
                return null;
            }
        };

        task.setOnSucceeded(workerStateEvent -> {
            if (canvasController != null) canvasController.updateCanvas();
            if (visualController != null) {visualController.setEventHandler();}
        });
        task.setOnFailed(workerStateEvent -> {
            if (canvasController != null) {canvasController.updateCanvas();}
            if (visualController != null) {visualController.setEventHandler();}
        });
        new Thread(task).start();
    }

    public void makeToast(String message) {
        String toastMsg = message;
        int toastMsgTime = 1500; //1.5 seconds
        int fadeInTime = 500; //0.5 seconds
        int fadeOutTime = 500; //0.5 seconds
        Toast.makeText(stage, toastMsg, toastMsgTime, fadeInTime, fadeOutTime);
    }

    private void playSlideAnimation(AnchorPane parentContainer, AnchorPane container, int k) {
        root.setPrefWidth(stage.getWidth());
        root.setPrefHeight(stage.getHeight() - 32);
        root.translateXProperty().set(k * currentScene.getWidth());
        parentContainer.getChildren().add(root);
        playAnim(parentContainer, container);
    }

    private void playAnim(AnchorPane parentContainer, AnchorPane container) {
        Timeline timeline = new Timeline();
        KeyValue keyValue = new KeyValue(root.translateXProperty(), 0, Interpolator.EASE_IN);
        // KeyValue keyValue = new KeyValue(root.translateXProperty(), 0, Interpolator.SPLINE(0.25, 0.1, 0.25, 1));
        KeyFrame keyFrame = new KeyFrame(Duration.seconds(0.3), keyValue);

        timeline.getKeyFrames().add(keyFrame);
        timeline.setOnFinished(animEvent -> {
            parentContainer.getChildren().remove(container);
        });

        timeline.play();
    }

    public RequestSender getSender() {
        return sender;
    }

    public Stage getStage() {
        return stage;
    }

    public User getUser() {
        return sender.getUser();
    }

    public void setUser(User user) {
        sender.setUser(user);
    }

    public Stack<MusicBand> getCollection() {
        if (collection == null)
            collection = (Stack<MusicBand>) sender.send(RequestCreator.getInstance().getCollectionRequest(sender.getUser()));
        return collection;
    }

    public void setCollection(Stack<MusicBand> collection) {
        this.collection = collection;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public void redrawScene() {
        if (mainController != null) mainController.redrawButtons();
        if (visualController != null) visualController.redrawButtons();
    }

    public void setAddController(AddController addController) {
        this.addController = addController;
    }

    public void setUpdateController(UpdateController updateController) {
        this.updateController = updateController;
    }

    public void setLanguageController(LanguageController languageController) {
        this.languageController = languageController;
    }

    public void setCommandsController(HeaderWindowController commandsController) {
        this.commandsController = commandsController;
    }

    public void setExecuteController(ExecuteController executeController) {
        this.executeController = executeController;
    }

    public void setHelpController(HeaderWindowController helpController) {
        this.helpController = helpController;
    }

    public void setTableViewController(TableViewController tableViewController) {
        this.tableViewController = tableViewController;
    }

    public void setVisualController(VisualController visualController) {
        this.visualController = visualController;
    }

    public AddController getAddController() {
        return addController;
    }

    public UpdateController getUpdateController() {
        return updateController;
    }

    public HeaderWindowController getLanguageController() {
        return languageController;
    }

    public HeaderWindowController getCommandsController() {
        return commandsController;
    }

    public HeaderWindowController getExecuteController() {
        return executeController;
    }

    public HeaderWindowController getHelpController() {
        return helpController;
    }

    public TableViewController getTableViewController() {
        return tableViewController;
    }

    public VisualController getVisualController() {
        return visualController;
    }

    public void setFilterController(FilterController<?> filterController) {
        this.filterController = filterController;
    }

    public FilterController getFilterController() {
        return this.filterController;
    }

    public void switchTable() {
        tableViewController.switchDisabledTable();
    }

    public void drawCanvas() {
        canvasController.start();
    }

    public CanvasController getCanvasController() {
        return canvasController;
    }

    public void setCanvasController(CanvasController canvasController) {
        this.canvasController = canvasController;
    }

    public static SpotifyAPI getSpotifyAPI() {
        return spotifyAPI;
    }
}