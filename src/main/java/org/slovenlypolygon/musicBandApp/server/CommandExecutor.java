package org.slovenlypolygon.musicBandApp.server;

import org.slovenlypolygon.musicBandApp.common.commands.Command;
import org.slovenlypolygon.musicBandApp.common.models.Album;
import org.slovenlypolygon.musicBandApp.common.models.MusicBand;
import org.slovenlypolygon.musicBandApp.common.models.MusicBandCollection;
import org.slovenlypolygon.musicBandApp.common.utils.Mark;
import org.slovenlypolygon.musicBandApp.common.utils.Response;
import org.slovenlypolygon.musicBandApp.common.utils.User;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;


/**
 * ЧЕЛ, КОТОРЫЙ УМЕЕТ ИСПОЛНЯТЬ КОМАНДЫ И ДРУЖИТ С КОЛЛЕКЦИЕЙ
 */
public class CommandExecutor {
    private final ReentrantLock lock = new ReentrantLock();
    private MusicBandCollection collection; // не финал, потому что изменяю ее в collection - database.load();
    private final DataBaseHelper database;

    public CommandExecutor(MusicBandCollection collection, DataBaseHelper database) {
        this.collection = collection;
        this.database = database;
    }

    public Response executeCommand(Command command, Object commandArgs, User user) {
        Response response = null;
        try {
            switch (command) {
                case IS_UPDATED:
                    lock.lock();
                    response = isUpdated((Stack<MusicBand>) commandArgs);
                    break;
                case REG:
                    lock.lock();
                    response = reg((User) commandArgs);
                    break;
                case LOG:
                    response = log((User) commandArgs);
                    break;


                case HELP:
                    response = help();
                    break;
                case INFO:
                    response = info();
                    break;
                case SHOW:
                    response = show();
                    break;
                case ADD:
                    lock.lock();
                    response = add((MusicBand) commandArgs, user);
                    break;
                case UPDATE_ID:
                    lock.lock();
                    response = updateId((AbstractMap.SimpleEntry<Integer, MusicBand>) commandArgs, user);
                    break;
                case REMOVE_BY_ID:
                    lock.lock();
                    response = removeId((int) commandArgs, user);
                    break;
                case CLEAR:
                    lock.lock();
                    response = clear(user);
                    break;
                case EXECUTE:
                    response = new Response("", "Скрипт был выполнен", Mark.SUCCESS);
                    break;
                case SHUFFLE:
                    response = shuffle();
                    break;
                case SORT:
                    response = sort();
                    break;
                case REORDER:
                    response = reorder();
                    break;
                case SHOW_BY_ALBUM:
                    response = showByBestAlbum((String) commandArgs);
                    break;
                case SHOW_GREATER_THAN_ALBUM:
                    response = showGreaterThanAlbum((Album) commandArgs);
                    break;
                case SHOW_NUM_OF_PARTICIPANTS:
                    response = showNumberOfParticipants();
                    break;
            }
        } finally {
            if (lock.isLocked()) {
                lock.unlock();
            }
        }

        return response;
    }

    private Response isUpdated(Stack<MusicBand> userCol) {
        if (!userCol.equals(collection.getCollection())) return new Response("Updated", null, Mark.UPDATE);
        else return new Response("Not updated", null, Mark.NOT_UPDATE);
    }

    private Response help() {
        return new Response("Справка:", "Возможные команды (Для авторизованных пользователей): \n" +
                "'help'                  : вывести справку по доступным командам\n" +
                "'info'                  : вывести информацию о коллекции\n" +
                "'show'                  : вывести все элементы коллекции\n" +
                "'add'                   : добавить новый элемент в коллекцию\n" +
                "'updateId X'            : обновить значение элемента коллекции (X - id элемента, которое нужно заменить)\n" +
                "'removeId X'            : удалить элемент из коллекции по его id (X - id элемента, которое нужно заменить)\n" +
                "'clear'                 : очистить коллекцию\n" +
                "'execute PATH'          : считать и исполнить скрипт из указанного файла.(PATH - путь до файла-скрипта)\n" +
                "'exit'                  : завершить программу (без сохранения в файл)\n" +
                "'shuffle'               : перемешать элементы коллекции в случайном порядке\n" +
                "'reorder'               : отсортировать коллекцию в порядке\n" +
                "'showByAlbum'           : вывести элементы, значение поля bestAlbum которых равно заданному\n" +
                "'showGreaterThanAlbum'  : вывести элементы, значение поля bestAlbum которых больше заданного\n" +
                "'showNumOfParticipants' : вывести значения поля numberOfParticipants всех элементов в порядке возрастания\n\n" +
                "'reg'                   : выйти из аккаунта и зарегистрировать новый\n" +
                "'log'                   : выйти из аккаунта и войти в другой" + "\n\n" +
                "Возможные команды (Для неавторизованных пользователей): \n" +
                "'help'                  : вывести справку по доступным командам\n" +
                "'reg'                   : зарегистрировать новый аккаунт\n" +
                "'log'                   : войти в аккаунт", Mark.SUCCESS);
    }

    private Response info() {
        return new Response("Информация о коллекции:", collection.getInfo(), Mark.SUCCESS);
    }

    private Response show() {
        return new Response("Коллекция:", collection.getCollection(), Mark.COLLECTION);
    }

    private Response add(MusicBand element, User user) {
        try {
            int id = database.getIdSeq();

            element.setId(id);
            element.setCreationDate(new Date());
            database.add(element, id, user);
            collection.addMusicBand(element);
        } catch (SQLException e) {
            Server.logger.info("Database work error");
            e.printStackTrace();
        }
        return new Response("", "Элемент добавлен в коллекцию", Mark.SUCCESS);
    }

    private Response updateId(AbstractMap.SimpleEntry<Integer, MusicBand> commandArgs, User user) {
        try {
            System.out.println(commandArgs);
            System.out.println(commandArgs.getKey());
            System.out.println(commandArgs.getValue());
            int id = commandArgs.getKey();
            MusicBandCollection newCol = database.updateById(id, commandArgs.getValue(), user);

            if (!newCol.equals(collection)) {
                collection = database.load();
                return new Response("Измененный элемент:", collection.getElementByID(id).toString(), Mark.SUCCESS);
            }
        } catch (SQLException e) {
            Server.logger.info("Database work error");
            e.printStackTrace();
        }
        return new Response("Элемента с таким id нет или элемент вам не принадлежит.", "Возврат в главное меню...", Mark.ERROR);
    }

    private Response removeId(int id, User user) {
        try {
            MusicBandCollection newCol = database.removeById(id, user);
            if (!newCol.equals(collection)) {
                String msg = collection.getElementByID(id).toString();
                collection = database.load();
                return new Response("Элемент, который был удален:", msg, Mark.SUCCESS);
            }
        } catch (SQLException e) {
            Server.logger.info("Database work error");
            e.printStackTrace();
        }
        return new Response("Элемента с таким id нет или элемент вам не принадлежит.", "Возврат в главное меню...", Mark.ERROR);
    }

    private Response clear(User user) {
        try {
            database.clear(user);
            collection = database.load();
            Server.logger.info("Command clear executed, collection clear");
        } catch (SQLException e) {
            Server.logger.info("Database work error");
            e.printStackTrace();
        }

        return new Response("Коллекция очищена от элементов, принадлежащих вам.", "Возврат в главное меню...", Mark.SUCCESS);
    }

    private Response shuffle() {
        collection.shuffle();
        return new Response("Коллекция перемешана", "Возврат в главное меню...", Mark.SUCCESS);
    }

    private Response sort() {
        Collections.sort(collection.getCollection());
        return new Response("Коллекция отсортирована по размеру объектов в ней", "Возврат в главное меню...", Mark.SUCCESS);
    }

    private Response reorder() {
        Collections.reverse(collection.getCollection());
        return new Response("Обратная сортировка произведена", "Возврат в главное меню...", Mark.SUCCESS);
    }

    private Response showByBestAlbum(String albumName) {
        return new Response("Результат:", collection.getCollection().stream().filter(element -> element.getBestAlbum().getName().equals(albumName)).collect(Collectors.toList()), Mark.COLLECTION);
    }

    private Response showGreaterThanAlbum(Album album) {
        List<MusicBand> list = collection.getCollection().stream().filter(element -> element.getBestAlbum().compareTo(album) > 0).collect(Collectors.toList());
        return new Response("Результат:", list, Mark.COLLECTION);
    }

    private Response showNumberOfParticipants() {
        return new Response("Общее количество участников:", collection.getCollection().stream().map(MusicBand::getNumberOfParticipants).sorted().collect(Collectors.toList()), Mark.COLLECTION);
    }

    private Response reg(User user) {
        try {
            boolean isContains = database.isContainsUserByLogin(user);

            if (!isContains) {
                database.addUser(user);
                return new Response("Вы успешно зарегистрировались >.<", user, Mark.USER);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new Response("Упс, пользователь с таким именем уже существует, возврат в главное меню >.<", null, Mark.USER_ERROR);
    }

    private Response log(User user) {
        boolean isContains = database.checkUser(user);

        if (isContains) {
            return new Response("Вы успешно вошли >.<", user, Mark.USER);
        }

        return new Response("Неправильный логин или пароль, возврат в главное меню >.<", null, Mark.USER_ERROR);
    }
}

