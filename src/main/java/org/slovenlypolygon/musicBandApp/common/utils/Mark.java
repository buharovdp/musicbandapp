package org.slovenlypolygon.musicBandApp.common.utils;

public enum Mark {
    SUCCESS,
    COLLECTION,
    UPDATE,
    NOT_UPDATE,
    ERROR,
    USER,
    USER_ERROR;
}
